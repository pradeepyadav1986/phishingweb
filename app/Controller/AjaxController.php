<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class AjaxController extends AppController {

    /**
     * Helpers
     *
     * @var array
     */
    public $helpers = array('Text', 'Js', 'Time');

    /**
     *
     * Model
     *  
     */
    public $uses = array('User','Category','UserSetting','Theme');

    /**
     * Components
     *
     * @var array
     */
    //public $components = array('Common', 'Schedule');
    public $components = array('Email');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('isDuplicateUserEmail','info','isDuplicateUserName');
    }

    /**
     * Admin process
     *
     * @return Bool
     * @access public
     */
    public function isDuplicateUserEmail() {
        $this->layout = false;
        $userExist = '';
        $conditions = array();
        if (isset($this->request->data['remoteId']) && $this->request->data['remoteId'] != "undefined") {
            $conditions['User.id !='] = $this->request->data['remoteId'];
        }
        if (isset($this->request->data['User']['email']) && !empty($this->request->data['User']['email'])) {
            $conditions['User.email'] = $this->request->data['User']['email'];
            $userExist = $this->User->find('count', array('conditions' => $conditions));
        }
        if ($userExist > 0) {
            echo "false";
        } else {
            echo "true";
        }
        $this->autoLayout = false;
        $this->autoRender = false;
    }
	 
	 public function isDuplicateUserName() {
		 
        $this->layout = false;
        $userExist = '';
        $conditions = array();
        if (isset($this->request->data['remoteId']) && $this->request->data['remoteId'] != "undefined") {
            $conditions['User.id !='] = $this->request->data['remoteId'];
        }
        if (isset($this->request->data['User']['username']) && !empty($this->request->data['User']['username'])) {
            $conditions['User.username'] = $this->request->data['User']['username'];
            $userExist = $this->User->find('count', array('conditions' => $conditions));
        }
        if ($userExist > 0) {
            echo "false";
        } else {
            echo "true";
        }
        $this->autoLayout = false;
        $this->autoRender = false;
    }

    /**
     * Admin process
     *
     * @return Bool
     * @access public
     */
    public function isDuplicateMembershipName() {
        $this->layout = false;
        $membershipExist = '';
        $conditions = array();
        if (isset($this->request->data['remoteId']) && $this->request->data['remoteId'] != "undefined") {
            $conditions['Membership.id !='] = $this->request->data['remoteId'];
        }
        if (isset($this->request->data['Membership']['name']) && !empty($this->request->data['Membership']['name'])) {
            $conditions['Membership.name'] = $this->request->data['Membership']['name'];
            $membershipExist = $this->Membership->find('count', array('conditions' => $conditions));
        }
        if ($membershipExist > 0) {
            echo "false";
        } else {
            echo "true";
        }
        $this->autoLayout = false;
        $this->autoRender = false;
    }

    /**
     * Admin process
     *
     * @return Bool
     * @access public
     */
    public function isDuplicateExerciseName() {
        $this->layout = false;
        $exerciseExist = '';
        $conditions = array();
        if (isset($this->request->data['remoteId']) && $this->request->data['remoteId'] != "undefined") {
            $conditions['Exercise.id !='] = $this->request->data['remoteId'];
        }
        if (isset($this->request->data['Exercise']['name']) && !empty($this->request->data['Exercise']['name'])) {
            $conditions['Exercise.name'] = $this->request->data['Exercise']['name'];
            $exerciseExist = $this->Exercise->find('count', array('conditions' => $conditions));
        }
        if ($exerciseExist > 0) {
            echo "false";
        } else {
            echo "true";
        }
        $this->autoLayout = false;
        $this->autoRender = false;
    }
    public function info() {
      $this->layout = false;
      $this->autoRender = false;
      $model=$this->request->data['model'];
      $field=$this->request->data['field'];
      $fieldValue=$this->request->data[$model][$field];
      $this->loadModel($model);
      $count=$this->$model->find('count',array('conditions'=>array($model.'.'.$field=>$fieldValue)));
      if($count==0){
       exit('true');
      }else{
       exit('false');   
      }      
    }
	
    function status() {
       $this->layout = false;
       $this->autoRender = false; 
       if ($this->request->is(array('post', 'put','ajax'))) {
       $model=$this->request->data['model'];
       $id=$this->request->data['model_id'];
       $status=$this->request->data['status'];      
       $this->loadModel($model);
       $this->$model->id=$id;
       $data[$model]['status']=$status;
       if($this->$model->save($data)){
		  if(isset($model) && $model=='User'){ 
		  $Userr = $this->User->find('first', array('conditions' => array('User.id' => $id)));
	      if($status==1 && !empty($Userr)){ 
		  $name = $Userr['User']['full_name'];
          $email = $Userr['User']['email'];
		  $this->loadModel('EmailTemplate');
		  $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'user_active')));
		  $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
          $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
          $subject = $email_template_detail['EmailTemplate']['subject'];
          $sender_name = $email_template_detail['EmailTemplate']['sender_name'];
		  $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
          $email_template = str_replace('[username]', $name, $email_template);      
		  $this->sendEmail($email, $sender_email, $subject, 'common', $email_template); 
		  }
		  }
           echo 'true';
       }else{
           echo 'false';
       }
       }else{
           $this->redirect('/');
       }
       
    }
	
	function verify() {
       $this->layout = false;
       $this->autoRender = false; 
       if ($this->request->is(array('post', 'put','ajax'))) {
       $model=$this->request->data['model'];
       $id=$this->request->data['model_id'];
       $status=$this->request->data['status'];      
       $this->loadModel($model);
       $this->$model->id=$id;
       $data[$model]['verified']=$status;
       if($this->$model->save($data)){
           echo 'true';
       }else{
           echo 'false';
       }
       }else{
           $this->redirect('/');
       }
       
    }
	
    function get_filter() {
        $this->layout = false;
        $this->autoRender = false; 
        if($this->request->is(array('post', 'put','ajax'))) {
          $this->Category->recursive = 3;
            $categories = $this->Category->find('first', array('conditions' => array('Category.status' => 1, 'Category.id' => $this->request->data['category'])));
            $temp = $subArr = $subArr2 = $subArr3 = $subArr4 = array();
            if (isset($categories['SubCategory']) && !empty($categories['SubCategory'])) {
                foreach ($categories['SubCategory'] as $category) {
                    $subArr = $subArr3 = $subArr4 = array();
                    if (!empty($category['SubCategory'])) {
                        foreach ($category['SubCategory'] as $value) {
                            $subArr4 == array();
                            $subArr4[$value['id']] = $value['name'];
                        }
                        $subArr2[$category['name']] = $subArr4;
                    }else{
                        $subArr2[$category['id']]=$category['name'];
                    }
                    
                }
            }
            
            if(!empty($subArr2)){
                $view = new View($this, false);
		$content = $view->element('filter', array('data'=>$subArr2)); 
		$sendArray['replyCode'] = 'success';
		$sendArray['content'] = $content;
            }else{
                $sendArray['replyCode'] = 'error';
		$sendArray['content'] = '<p class="no-filter">No Filter for this category.</p>';
            }
            echo json_encode($sendArray);
           // pr($subArr2);
        }
    }
    function change_settings() {
         $this->layout = false;
         $this->autoRender = false; 
         $user_setting=$this->UserSetting->findByTypeAndUserId($this->request->data['type'],  $this->Session->read('Auth.User.id'));
         if(!empty($user_setting)){
            $result = $this->UserSetting->updateAll(array('UserSetting.theme_id'=>$this->request->data['theme']),array('UserSetting.user_id'=>$this->Session->read('Auth.User.id'),'UserSetting.type'=>$this->request->data['type']));
         }else{
             $user_setting['UserSetting']['theme_id']=$this->request->data['theme'];
             $user_setting['UserSetting']['user_id']=$this->Session->read('Auth.User.id');
             $user_setting['UserSetting']['type']=$this->request->data['type'];
             $result = $this->UserSetting->save($user_setting);
         }
         $theme=$this->Theme->findById($this->request->data['theme'],array('Theme.id','Theme.image'));         
         if($result){
           $arr=array('replyCode'=>1,'contentId'=>$theme['Theme']['id'],'content'=>$theme['Theme']['image']);
         }else{
           $arr=array('replyCode'=>0,'contentId'=>'','content'=>'');   
         }
      echo json_encode($arr);   
    } 
    
    function validate_category_slug() {
        $this->layout = false;
        $this->autoRender = false; 
        
		$conditions = array();
        $slug=  str_replace('--', '-', preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', trim(strtolower($this->request->data['Category']['name'])))));
        
		$conditions[] = array('Category.slug'=>$slug);
		
		if(isset($this->request->data['Category']['id'])){
			$id = $this->request->data['Category']['id'];
			$conditions[] = array('Category.id <>'=>$id);
		}
		$slugExist = $this->Category->find('count',array('conditions'=>$conditions));
        
		if($slugExist){
            echo 'false'; 
        }else{
            echo 'true';
        } 
        exit;
     }

     function featuredPost() {
       $this->layout = false;
       $this->autoRender = false; 
       if ($this->request->is(array('post', 'put','ajax'))) {
           $model=$this->request->data['model'];
           $id=$this->request->data['model_id'];
           $status=$this->request->data['status'];      
           $this->loadModel($model);
           $this->$model->id=$id;
           $data[$model]['is_featured']=$status;
           
           if($this->$model->save($data)){
               echo 'true';
           }else{
               echo 'false';
           }
       }else{
           $this->redirect('/');
       }
       
    }
	
	public function isDuplicateCheckInCode() {
        $this->layout = false;
        $userExist = '';
        $conditions = array();
        if (isset($this->request->data['remoteId']) && $this->request->data['remoteId'] != "undefined") {
            $conditions['User.id !='] = $this->request->data['remoteId'];
        }
        if (isset($this->request->data['User']['check_in_code']) && !empty($this->request->data['User']['check_in_code'])) {
            $conditions['User.check_in_code'] = $this->request->data['User']['check_in_code'];
            $userExist = $this->User->find('count', array('conditions' => $conditions));
        }
        if ($userExist > 0) {
            echo "false";
        } else {
            echo "true";
        }
        $this->autoLayout = false;
        $this->autoRender = false;
    }
	
}
