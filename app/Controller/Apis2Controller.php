<?php

App::uses('Sanitize', 'Utility');

class Apis2Controller extends AppController {

    public $name = 'Users';
    public $uses = array('User','Client', 'DomainInfo','DomainRegistrar','DomainRegistrantContact','VirusTotal','VirusTotalScan','Dnstwist','CatchPhishing','CatchPhishingCompany','EmailTemplate');
    public $helpers = array('Html', 'Form', 'Session','Common');
    public $components = array('Cookie', 'Email','Json','Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow('login','signup','client_list','client_add','recently_added_domain','add_domain_registrar','add_domain_info','add_domain_registrant_contact','add_virus_total_info','add_virus_total_scan', 'add_dns_twist','add_virus_total_scan_info','add_catch_phishing','add_catch_phishing_company','scan_result','impersonated_domain_locations','dashboard_count');
		$this->Auth->allow();
    }
	
	
	public function login() {
        $this->layout = false;
        $this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put')) {
		  $email =$this->request->data['email'];
		  $password =$this->request->data['password'];
		  $this->request->data['User']['email'] = $this->request->data['email'];
		  $this->request->data['User']['password'] = $this->request->data['password'];
		  $userData = $this->User->find('first',array('conditions'=>array('User.email'=>$email)));
			if(empty($userData)){
				 $msg='User not exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
			    /*echo "<pre>";
                print_r($userData);
                die;*/
                if($userData['User']['verified'] == 0){
                  $msg='You are not verify your email address. Please Verify';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}
                if($userData['User']['status'] == 0){
                  $msg='You account has been deactivated. Please contact Administration.';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}				
				if ($this->Auth->login()) {
					//$user = $this->Auth->identify();
					$user = $this->Session->read('Auth.User');
					$user_id  = $this->Session->read('Auth.User.id');
					//$sid = AuthComponent::password($user_id);
					$sid = trim(base64_encode($user_id));
					//$data = array("user"=> $user, "sid"=> $sid);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Login Successfully',"data"=>$user,"sid"=>$sid);
                    return json_encode($arr);
				} else {
					 $msg='Invalid username or password.';
					 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					 return json_encode($arr);
				}
			}
		}
    }
	
	public function signup() {
        $this->layout = false;
        $this->autoRender = false;
		//echo 'Sign Up';	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['User']['name']=$this->request->data['name'];
			$this->request->data['User']['email']=$this->request->data['email'];
			$this->request->data['User']['password'] = AuthComponent::password($this->request->data['password']);
			$this->request->data['User']['status']=1;
			$name = $this->request->data['User']['name'];
			$email = $this->request->data['User']['email'];
			//echo "<pre>";
			//print_r($this->request->data);die;
			$email = $this->request->data['email'];
			$userData = $this->User->find('count',array('conditions'=>array('User.email'=>$email)));
			if($userData >= 1){
				 $msg='Email allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->User->save($this->request->data, false)) {
					$lastInsertId = $this->User->getLastInsertId();
					$activationKey = trim(base64_encode($lastInsertId));
					$email_template = '<table border="1" cellpadding="1" cellspacing="1" style="width:80%">
									<tbody>
										<tr>
											<td><strong>Hello [username],</strong></td>
										</tr>
										<tr>
											<td>
											<p>Welcome! You have successfully registered.\r\n</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Please verify your account by clicking bellow link.\r\n</p>
											<p>[link]</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Thanks!</p>

											<p>[site_title]</p>
											<p></p>
											</td>
										</tr>
									</tbody>
								</table>';
					//print_r($email_template);die;
					$url1 = Router::url(array('controller' => 'apis', 'action' => 'verify',$activationKey), true);
                    $url = "<a href='$url1'>Click Here</a>";			
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
					$email_template = str_replace('[link]', $url, $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    //$email_template = str_replace('[password]', $pass, $email_template);
					$subject = 'Registration  Email';
					$sender_email = 'info@phishing.com';
					 /*$this->sendEmail($email, $sender_email, $subject, 'common', $email_template);
					 $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'User Created Successfully');
					 return json_encode($arr);*/
					try{
                             $this->sendEmail($email, $sender_email, $subject, 'common', $email_template);
                             $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'User Created Successfully');
					         return json_encode($arr);
                      }catch(Exception $e){
                            $msg='Something went wrong! Please try again.';
							$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
							return json_encode($arr);
                        }
					
				}else{
					$msg='Something went wrong! Please try again.';
					$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					return json_encode($arr);
					}
			}	
        }
    }
	
	 public function forgot_password() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['email'];
            if (!empty($email)) {
                $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'forgot_password')));

                $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
                $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
                $subject = $email_template_detail['EmailTemplate']['subject'];
                $sender_name = $email_template_detail['EmailTemplate']['sender_name'];

                $Userr = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if (!empty($Userr)) {
                    $newpassword = $this->__generatePassword();
                    $Userr['User']['password'] = $this->Auth->password($newpassword);
                    $Userr['User']['id'];

                    $name = $Userr['User']['name'];
                    $email = $Userr['User']['email'];
                    $pass = $newpassword;
					
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[fullname]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    $email_template = str_replace('[PASSWORD]', $pass, $email_template);
					
					//try{
                            $this->sendEmail($this->request->data['email'], $sender_email, $subject, 'common', $email_template);
                             if ($this->User->Save($Userr['User'])) {
								$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Please check your email for reset password.'); 
		    					echo json_encode($arr); die; 
							}else{	
								$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'unable to send email.'); 
		    					echo json_encode($arr); die;
							}
                        /*}catch(Exception $e){
                            $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'unable to send email.'.$e); 
		    				echo json_encode($arr); die;
                        }*/
						
                } else {
                    $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'No user found.'); 
		    		echo json_encode($arr); die; 
                }
            } else {
                $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'No email found.'); 
		    	echo json_encode($arr); die;
            }
        }
    }
	
	public function client_list() {
        $this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('put')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			$result=array();
			$client = $this->Client->find('all',array('conditions'=>array('Client.user_id'=>$user_id),'order'=>'Client.id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client List',"data"=>$client); 
		    echo json_encode($arr); die; 
        }		
    }
	
	public function client_add() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			$client_name = $this->request->data['client_name'];
			$industry = $this->request->data['industry'];
			$official_website = $this->request->data['official_website'];
			$domain_name = $this->request->data['domain_name'];
			if(!empty($this->request->data['id'])){
				$this->request->data['Client']['id'] = $this->request->data['id'];
			}
			$this->request->data['Client']['user_id']=$user_id;
			$this->request->data['Client']['client_name']=$client_name;
			$this->request->data['Client']['industry'] = $industry;
			$this->request->data['Client']['official_website']= $official_website;
			$this->request->data['Client']['domain_name']= $domain_name;
			$clientData = $this->Client->find('count',array('conditions'=>array('Client.client_name'=>$client_name)));
			if($clientData >= 1 && empty($this->request->data['id'])){
				 $msg='This Client allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->Client->save($this->request->data, false)) {
					$clientid = $this->Client->getLastInsertID();
					$clientData  = $this->Client->findById($clientid);
					if(empty($this->request->data['id'])){
						if(exec("/usr/bin/python /var/www/html/python/dnstwist.py ".$domain_name."___".$clientid)){
							$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ',"clientData"=>$clientData);
						}else{
							$arr = array("statusCode" => "400", "replyStatus" => "success", "replyMessage" =>'Client Added Successfully, but python not executed',"clientData"=>$clientData);
						}
					}else{
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ',"clientData"=>$this->request->data);
					}
					
					return json_encode($arr);
				}	
			}	
        }
    }
	
	public function recently_added_domain($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $domains = $this->DomainInfo->find('all',array('conditions'=>array('DomainInfo.client_id'=>$client_id),'limit' => 12,'order'=>'DomainInfo.id desc'));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Info List',"data"=>$domains); 
		echo json_encode($arr); die;  		
	}

	
	public function scan_result(){
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->Dnstwist->bindModel(
			array('hasOne'=>array(
				'VirusTotal' => array(
					'className' => 'VirusTotal',
					'foreignKey' => false,
					'conditions' => array(
						'Dnstwist.domain_name = VirusTotal.resource',
					))
		)));

		$this->Dnstwist->bindModel(
			array('hasOne'=>array(
				'DomainInfo' => array(
					'className' => 'DomainInfo',
					'foreignKey' => false,
					'conditions' => array(
						'Dnstwist.domain_name = DomainInfo.domain_name',
					))
		)));
        //$dnstwist = $this->Dnstwist->find('all',array('conditions'=>array('Dnstwist.client_id'=>$client_id),'limit'=>10,'order'=>'Dnstwist.id DESC'));
		$conditions = array();
		if ($this->request->is('post') || $this->request->is('put')) {
			 $client_id = $this->request->data['client_id'];
			 $conditions[] = array('Dnstwist.client_id'=>$client_id,'Dnstwist.dns_a !='=>'');
		}	
		$this->paginate = array('conditions'=>$conditions, 'limit' => 10,'order'=>'Dnstwist.id asc');
        $dnstwist = $this->paginate('Dnstwist');
		//echo "<pre>";
		//print_r($dnstwist);die;
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist List',"data"=>$dnstwist); 
		echo json_encode($arr); die;  		
	}
	
	public function impersonated_domain_locations($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->DomainInfo->bindModel(array('hasOne'=>array(
		'DomainRegistrantContact' => array(
            'className' => 'DomainRegistrantContact',
            'foreignKey' => false,
            'conditions' => array(
                'DomainInfo.id = DomainRegistrantContact.domain_id',
            )))));
        $domainInfo = $this->DomainInfo->find('all',array('fields'=>array('DomainRegistrantContact.mailing_address','DomainRegistrantContact.city_name','DomainRegistrantContact.state_name','DomainRegistrantContact.country_name'),'conditions'=>array('DomainInfo.client_id'=>$client_id)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'DomainInfo List',"data"=>$domainInfo); 
		echo json_encode($arr); die;  		
	}

	
	
	public function dashboard_count($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        // $impersonatedSites = $this->DomainInfo->find('count',array('conditions'=>array('DomainInfo.client_id'=>$client_id)));
        $impersonatedSites = $this->Dnstwist->find('count',array('conditions'=>array('Dnstwist.client_id'=>$client_id)));
        $infectedSites = $this->VirusTotal->find('count',array('conditions'=>array('VirusTotal.positives !='=>0,'VirusTotal.client_id'=>$client_id)));
        $unregisterSites = $this->VirusTotal->find('count',array('conditions'=>array('VirusTotal.positives !='=>0,'VirusTotal.client_id'=>$client_id)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Info List',"impersonatedSites"=>$impersonatedSites,"infectedSites"=>$infectedSites,"unregisterSites"=>$unregisterSites); 
		echo json_encode($arr); die;  		
	}
	
	public function add_domain_info($client_id= null) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$domain_name = $this->request->data['domain_name'];
			$query_time = $this->request->data['query_time'];
			$whois_server = $this->request->data['whois_server'];
			$domain_registered = $this->request->data['domain_registered'];
			$create_date = $this->request->data['create_date'];
			$update_date = $this->request->data['update_date'];
			$expiry_date = $this->request->data['expiry_date'];
			$status = $this->request->data['status'];
			
			$name_servers ='';
			$domain_status = '';
			$api_credits_charged = '';
			$whois_retrieval_time = '';
			$user_ip_address = '';
			if(!empty($this->request->data['name_servers'])){
				$name_servers = implode(",",$this->request->data['name_servers']);
			}
            if(!empty($this->request->data['domain_status'])){
				$domain_status = implode(",",$this->request->data['domain_status']);
			}
			if(!empty($this->request->data['query_stats']['api_credits_charged'])){
               $api_credits_charged = $this->request->data['query_stats']['api_credits_charged'];
			}   
			if(!empty($this->request->data['query_stats']['whois_retrieval_time'])){
               $whois_retrieval_time = $this->request->data['query_stats']['whois_retrieval_time'];
			}   
			if(!empty($this->request->data['query_stats']['user_ip_address'])){
               $user_ip_address = $this->request->data['query_stats']['user_ip_address'];
			}   
			$this->request->data['DomainInfo']['client_id']=$client_id;
			$this->request->data['DomainInfo']['domain_name']=$domain_name;
			$this->request->data['DomainInfo']['query_time']=$query_time;
			$this->request->data['DomainInfo']['whois_server'] = $whois_server;
			$this->request->data['DomainInfo']['domain_registered']= $domain_registered;
			$this->request->data['DomainInfo']['create_date']= $create_date;
			$this->request->data['DomainInfo']['update_date']= $update_date;
			$this->request->data['DomainInfo']['expiry_date']= $expiry_date;
			$this->request->data['DomainInfo']['status']= $status;
			$this->request->data['DomainInfo']['name_servers']= $name_servers;
			$this->request->data['DomainInfo']['domain_status']= $domain_status;
			$this->request->data['DomainInfo']['api_credits_charged']= $api_credits_charged;
			$this->request->data['DomainInfo']['whois_retrieval_time']= $whois_retrieval_time;
			$this->request->data['DomainInfo']['user_ip_address']= $user_ip_address;
			$domainData = $this->DomainInfo->find('count',array('conditions'=>array('DomainInfo.domain_name'=>$domain_name)));
			if($domainData >= 1){
				 $msg='This domain name allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->DomainInfo->save($this->request->data, false)) {
					$lastInsertId = $this->DomainInfo->getLastInsertId();
					if($this->request->data['domain_registrar']) $this->add_domain_registrar($this->request->data['domain_registrar'],$lastInsertId);
					if($this->request->data['registrant_contact']) $this->add_domain_registrant_contact($this->request->data['registrant_contact'],$lastInsertId);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Added Successfully');
					return json_encode($arr);
				}	
			}	
        }
    }
	
	public function add_domain_registrar($data,$domain_id) {
        $this->layout = false;
        $this->autoRender = false;	
        // if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $domain_id = $data['domain_id'];
			$iana_id = $data['iana_id'];
			$registrar_name = $data['registrar_name'];
			$whois_server = $data['whois_server'];
			$website_url = $data['website_url'];
			$email_address = $data['email_address'];
			$phone_number = $data['phone_number'];

			$this->request->data['DomainRegistrar']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrar']['iana_id']=$iana_id;
			$this->request->data['DomainRegistrar']['registrar_name'] = $registrar_name;
			$this->request->data['DomainRegistrar']['whois_server']= $whois_server;
			$this->request->data['DomainRegistrar']['website_url']= $website_url;
			$this->request->data['DomainRegistrar']['email_address']= $email_address;
			$this->request->data['DomainRegistrar']['phone_number']= $phone_number;
			if ($this->DomainRegistrar->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrar Successfully');
				return json_encode($arr);
			}		
        // }
    }
	
	public function add_domain_registrant_contact($data,$domain_id) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $domain_id = $this->request->data['domain_id'];
			$full_name = $data['full_name'];
			$company_name = $data['company_name'];
			$mailing_address = $data['mailing_address'];
			$city_name = $data['city_name'];
			$state_name = $data['state_name'];
			$zip_code = $data['zip_code'];
			$country_name = $data['country_name'];
			$country_code = $data['country_code'];
			$email_address = $data['email_address'];
			$phone_number = $data['phone_number'];
			$fax_number = $data['fax_number'];
			$this->request->data['DomainRegistrantContact']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrantContact']['full_name']=$full_name;
			$this->request->data['DomainRegistrantContact']['company_name'] = $company_name;
			$this->request->data['DomainRegistrantContact']['mailing_address']= $mailing_address;
			$this->request->data['DomainRegistrantContact']['city_name']= $city_name;
			$this->request->data['DomainRegistrantContact']['state_name']= $state_name;
			$this->request->data['DomainRegistrantContact']['zip_code']= $zip_code;
			$this->request->data['DomainRegistrantContact']['country_name']= $country_name;
			$this->request->data['DomainRegistrantContact']['country_code']= $country_code;
			$this->request->data['DomainRegistrantContact']['email_address']= $email_address;
			$this->request->data['DomainRegistrantContact']['phone_number']= $phone_number;
			$this->request->data['DomainRegistrantContact']['fax_number']= $fax_number;
			if ($this->DomainRegistrantContact->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrant Contact Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	
	public function add_catch_phishing() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			
			$score = $this->request->data['score'];

			if($score>=90)
				$status = 'Suspicious';
			elseif($score>=80)
				$status = 'Likely';
			elseif($score>=65)
				$status = 'Potential';
			
			
			$domain = $this->request->data['domain'];
			$this->request->data['CatchPhishing']['status']=$status;
			$this->request->data['CatchPhishing']['score']=$score;
			$this->request->data['CatchPhishing']['domain'] = $domain;
			if ($this->CatchPhishing->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Catch Phising Added Successfully.');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_catch_phishing_company() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
				
			$score = $this->request->data['score'];
			if($score>=90)
				$status = 'Suspicious';
			elseif($score>=80)
				$status = 'Likely';
			elseif($score>=65)
				$status = 'Potential';
			$domain = $this->request->data['domain'];
			$company = $this->request->data['company'];

			$this->request->data['CatchPhishingCompany']['status']=$status;
			$this->request->data['CatchPhishingCompany']['score']=$score;
			$this->request->data['CatchPhishingCompany']['domain'] = $domain;
			$this->request->data['CatchPhishingCompany']['company'] = $company;
			if ($this->CatchPhishingCompany->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Catch Phising Added Successfully.');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_virus_total_info($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $client_id =1;
			// if(!empty($this->request->data['client_id'])){
			// 	$client_id = $this->request->data['client_id'];
			// }	
			$scan_id = $this->request->data['scan_id'];
			$resource = $this->request->data['resource'];
			$url  = $this->request->data['url'];
			$scan_date = $this->request->data['scan_date'];
			$permalink  = $this->request->data['permalink'];
			$positives = $this->request->data['positives'];
			$total = $this->request->data['total'];
			$scans = $this->request->data['scans'];
			$this->request->data['VirusTotal']['client_id']=$client_id;
			$this->request->data['VirusTotal']['scan_id']=$scan_id;
			$this->request->data['VirusTotal']['resource'] = $resource;
			$this->request->data['VirusTotal']['url']= $url;
			$this->request->data['VirusTotal']['scan_date']= $scan_date;
			$this->request->data['VirusTotal']['permalink']= $permalink;
			$this->request->data['VirusTotal']['positives']= $positives;
			$this->request->data['VirusTotal']['total']= $total;
			// $this->request->data['VirusTotal']['scans']= $scans;
			if ($this->VirusTotal->save($this->request->data, false)) {
				$lastInsertId = $this->VirusTotal->getLastInsertId();
				$this->add_virus_total_scan($this->request->data['scans'],$lastInsertId);
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Virus total scan Added Successfully');
				return json_encode($arr);
			}		
        }
	}
	
	public function add_virus_total_scan($data1,$id){
		$this->layout = false;
        $this->autoRender = false;
		$this->request->data['VirusTotalScan']['virus_total_scan_id'] = $id;
        $keys = array_keys($data1);
        $count=0;
        foreach($data1 as $key){
			$this->VirusTotalScan->create(false);
			$this->request->data['VirusTotalScan']['site'] = $keys[$count];
			$this->request->data['VirusTotalScan']['detected'] = $key['detected']?'TRUE':'FALSE';
			$this->request->data['VirusTotalScan']['result'] = $key['result'];
			$this->VirusTotalScan->saveAll($this->request->data['VirusTotalScan']);
            $count++;
        } 
	}
	
	/*public function add_virus_total_scan_info() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$virus_total_scan_id =1;
			if(!empty($this->request->data['virus_total_scan_id'])){
				$virus_total_scan_id = $this->request->data['virus_total_scan_id'];
			}	
			$site = $this->request->data['site'];
			$detected = $this->request->data['detected'];
			$result = $this->request->data['result'];
			$this->request->data['VirusTotalScan']['virus_total_scan_id']=$virus_total_scan_id;
			$this->request->data['VirusTotalScan']['site']=$site;
			$this->request->data['VirusTotalScan']['detected'] = $detected;
			$this->request->data['VirusTotalScan']['result']= $result;
			if ($this->VirusTotalScan->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Virus Total Scan Info Added Successfully.');
				return json_encode($arr);
			}		
        }
    } */
	
	
	public function add_dns_twist() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$fuzzer  = $this->request->data['fuzzer'];
			$domain_name = $this->request->data['domain-name'];
			$dns_a  = $this->request->data['dns-a']?implode(",",$this->request->data['dns-a']):'';
			$this->request->data['Dnstwist']['client_id']=$client_id;
			$this->request->data['Dnstwist']['fuzzer']=$fuzzer;
			$this->request->data['Dnstwist']['domain_name'] = $domain_name;
			$this->request->data['Dnstwist']['dns_a']= $dns_a;
			if ($this->Dnstwist->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	
	public function graphApi() {
		$this->layout = false;
        $this->autoRender = false;	
		if ($this->request->is('post') || $this->request->is('put')) {
			$client_id = $this->request->data['client_id'];
			$mode = $this->request->data['mode'];
			//$impersonatedSites = $this->Dnstwist->find('count',array('conditions'=>array('Dnstwist.client_id'=>$client_id)));	
			$conditions = array('Dnstwist.client_id'=>$client_id);
			$monthUsersCount=array();
			$usersList = array();
			if($mode =='monthly'){
			   $usersbymonth = $this->Dnstwist->find('all',array('conditions'=>array('YEAR(Dnstwist.created)' => date('Y'), $conditions ), 'group'=>array('MONTH(Dnstwist.created)'), 'fields'=>array('Count(Dnstwist.id) as count', 'MONTH(Dnstwist.created) as month'), 'order'=>array('Dnstwist.created'=>'ASC')));
			   //$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                //echo "<pre>";
                //print_r($log);die;
				if(!empty($usersbymonth)){
					$fullMonthList = array(1=>'Jan',2=>'Feb',3=>'March',4=>'April',5=>'may',6=>'june',7=>'July',8=>'august',9=>'September',10=>'October',11=>'November',12=>'December');
					foreach($usersbymonth as $usersbymonths){
						$monthUsersCount[$usersbymonths[0]['month']] = $usersbymonths[0]['count'];
					}
					foreach($fullMonthList as $key=>$vel){
						//if($key <= date('m')){
							if(in_array($key, array_keys($monthUsersCount))){
								$usersList[] = $monthUsersCount[$key];
							}else{
								$usersList[] = 0;
							}
						//}
					}
					//$usersList = implode(',', $usersList); 
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
			}elseif($mode =='weekly'){	
			    $weekUsersCount=array();
				$usersbyweek = $this->Dnstwist->find('all',array('conditions'=>array('MONTH(Dnstwist.created)' => date('m'), $conditions ),'group'=>array('WEEK(Dnstwist.created)'), 'fields'=>array('Count(Dnstwist.id) as count', 'WEEK(Dnstwist.created) as week'), 'order'=>array('Dnstwist.created'=>'ASC')));
				//$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                //echo "<pre>";
                //print_r($log);die;
				$fullWeekList = array(1=>'First',2=>'Second',3=>'Third',4=>'Fourth',5=>'Fifth');
				foreach($usersbyweek as $usersbyweeks){
					$usersbyweeks[0]['week'] = $usersbyweeks[0]['week'] % 7;
					$weekUsersCount[$usersbyweeks[0]['week']] = $usersbyweeks[0]['count'];
				}
				foreach($fullWeekList as $key=>$vel){
					//if($key <= date('m')){
						if(in_array($key, array_keys($weekUsersCount))){
							$usersList[] = $weekUsersCount[$key];
						}else{
							$usersList[] = 0;
						}
					//}
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
				//echo "<pre>";
				//print_r($usersList);
				//die;
			}else{
				$yearUsersCount=array();
				$fullYearList = array();
				$currentyear = date('Y');
				$last10year = date('Y') - 10;
				$usersbyyear = $this->Dnstwist->find('all',array('conditions'=>array('YEAR(Dnstwist.created) >' => $last10year, $conditions ),'group'=>array('YEAR(Dnstwist.created)'), 'fields'=>array('Count(Dnstwist.id) as count', 'YEAR(Dnstwist.created) as year'), 'order'=>array('Dnstwist.created'=>'ASC')));
			     //$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                 //echo "<pre>";
                 //print_r($log);die;
				 for($i=$last10year+1; $i <= $currentyear; $i++){
					 $fullYearList[$i] = $i;
				 }	 
				 //echo "<pre>";
				 //print_r($usersbyyear);die;
                 //print_r($fullYearList);die;
				foreach($usersbyyear as $usersbyyears){
					$yearUsersCount[$usersbyyears[0]['year']] = $usersbyyears[0]['count'];
				}
				//echo "<pre>";
                //print_r($yearUsersCount);die;
				foreach($fullYearList as $key=>$vel){
					//if($key <= date('m')){
						if(in_array($key, array_keys($yearUsersCount))){
							$usersList[] = $yearUsersCount[$key];
						}else{
							$usersList[] = 0;
						}
					//}
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
			}				
		}			
	}
	
	public function get_whois_data($domain_id){
		$this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->DomainInfo->bindModel(
			array('hasOne'=>array(
				'DomainRegistrantContact' => array(
					'className' => 'DomainRegistrantContact',
					'foreignKey' => false,
					'conditions' => array(
						'DomainInfo.id = DomainRegistrantContact.domain_id',
					))
		)));

		$this->DomainInfo->bindModel(
			array('hasOne'=>array(
				'DomainRegistrar' => array(
					'className' => 'DomainRegistrar',
					'foreignKey' => false,
					'conditions' => array(
						'DomainInfo.id = DomainRegistrar.domain_id',
					))
			))
		);
        $domainInfo = $this->DomainInfo->find('first',array('conditions'=>array('DomainInfo.id'=>$domain_id)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'DomainInfo List',"data"=>$domainInfo); 
		echo json_encode($arr); die; 
	}
	
	public function logout() {
        $this->Session->setFlash('Logout Successfully.','success');
        $this->Auth->logout();
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

   public function verify($userid){
	    $this->layout = false;
        $this->autoRender = false;	
        $userId = base64_decode($userid);
        $userData['User']['id'] = $userId;
        $userData['User']['verified'] = 1;
        $checkuser = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
        if($checkuser){
            if ($this->User->save($userData['User'], false)) {
                $sendarray = array("statusCode"=>200,"replyMessage" => "User verified successfully.","usre_id"=>$userid);
            }else{
                $sendarray = array("statusCode"=>201,"replyMessage" => "User not verified successfully.");
            }
        }else{
            $sendarray = array("statusCode"=>201,"replyMessage" => "User not exist.");
        }
         
        echo json_encode($sendarray);
    }
	
	public function base64toimage($imagecode,$destination) {
       // Image upload /
        if (strpos($imagecode, "png;base64,") != 0)
            $filename = md5(rand()) . ".png";
        else
            $filename = md5(rand()) . ".jpeg";

		$dirpath = $destination;
        $mainImgDirPath = $dirpath . DS . $filename;
        $data_new = $imagecode;
        $pos = strpos($data_new, "base64,");
        if (isset($pos) && $pos != "") {
            $img = substr($data_new, $pos + 7, strlen($data_new));
        } else {
            $img = $data_new;
        }
        $img = str_replace(' ', '+', $img);
        $svdata = base64_decode($img);

        $fp = fopen($mainImgDirPath, 'w');
        if (fwrite($fp, $svdata)) {
            fclose($fp);
        } else {
            $mainImgDirPath = '';
        }
        return $filename;
        // Image upload End /
    }
	
   
	
  public function getToken($length){
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(50);
        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        return $token;
   }
	
	
}
