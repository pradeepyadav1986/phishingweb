<?php

App::uses('Sanitize', 'Utility');

class ApisController extends AppController {

    public $name = 'Users';
	public $uses = array('User','Client', 'ClientAppinfo','ClientApk','People','PhishingReport','DomainInfo','DomainRegistrar','DomainRegistrantContact','VirusTotal','VirusTotalScan','Dnstwist','CatchPhishing','CatchPhishingCompany','EmailTemplate',
'Setting','Pastebin','Punicode','TwitterData','InstagramData','Site','GithubData');
    public $helpers = array('Html', 'Form', 'Session','Common');
    public $components = array('Cookie', 'Email','Json','Upload');

    public function beforeFilter() {
		parent::beforeFilter();
		$this->response->header('Access-Control-Allow-Origin','*');
        $this->response->header('Access-Control-Allow-Methods','*');
        $this->response->header('Access-Control-Allow-Headers','X-Requested-With');
        $this->response->header('Access-Control-Allow-Headers','Content-Type, x-xsrf-token');
        $this->response->header('Access-Control-Max-Age','172800');
        //$this->Auth->allow('login','signup','client_list','client_add','recently_added_domain','add_domain_registrar','add_domain_info','add_domain_registrant_contact','add_virus_total_info','add_virus_total_scan', 'add_dns_twist','add_virus_total_scan_info','add_catch_phishing','add_catch_phishing_company','scan_result','impersonated_domain_locations','dashboard_count');
		$this->Auth->allow();
		
    }
	
	
	public function login() {
        $this->layout = false;
        $this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put')) {
		  $email =$this->request->data['email'];
		  $password =$this->request->data['password'];
		  $this->request->data['User']['email'] = $this->request->data['email'];
		  $this->request->data['User']['password'] = $this->request->data['password'];
		  $userData = $this->User->find('first',array('conditions'=>array('User.email'=>$email)));
			if(empty($userData)){
				 $msg='User not exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
			    /*echo "<pre>";
                print_r($userData);
                die;*/
                if($userData['User']['verified'] == 0){
                  $msg='You are not verify your email address. Please Verify';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}
                if($userData['User']['status'] == 0){
                  $msg='You account has been deactivated. Please contact Administration.';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}				
				if ($this->Auth->login()) {
					//$user = $this->Auth->identify();
					$user = $this->Session->read('Auth.User');
					$user_id  = $this->Session->read('Auth.User.id');
					//$sid = AuthComponent::password($user_id);
					$sid = trim(base64_encode($user_id));
					//$data = array("user"=> $user, "sid"=> $sid);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Login Successfully',"data"=>$user,"sid"=>$sid);
                    return json_encode($arr);
				} else {
					 $msg='Invalid username or password.';
					 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					 return json_encode($arr);
				}
			}
		}
    }
	
	public function signup() {
        $this->layout = false;
        $this->autoRender = false;
		//echo 'Sign Up';	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['User']['name']=$this->request->data['name'];
			$this->request->data['User']['email']=$this->request->data['email'];
			$this->request->data['User']['password'] = AuthComponent::password($this->request->data['password']);
			$this->request->data['User']['status']=1;
			$name = $this->request->data['User']['name'];
			$email = $this->request->data['User']['email'];
			//echo "<pre>";
			//print_r($this->request->data);die;
			$email = $this->request->data['email'];
			$userData = $this->User->find('count',array('conditions'=>array('User.email'=>$email)));
			if($userData >= 1){
				 $msg='Email allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->User->save($this->request->data, false)) {
					$lastInsertId = $this->User->getLastInsertId();
					$activationKey = trim(base64_encode($lastInsertId));
					$email_template = '<table border="0" cellpadding="1" cellspacing="1" style="width:80%">
									<tbody>
										<tr>
											<td><strong>Hello [username],</strong></td>
										</tr>
										<tr>
											<td>
											<p>Welcome! You have successfully registered.</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Please verify your account by clicking bellow link.</p>
											<p>[link]</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Thanks!</p>

											<p>[site_title]</p>
											<p></p>
											</td>
										</tr>
									</tbody>
								</table>';
					//print_r($email_template);die;
					// $url1 = Router::url(array('controller' => 'apis', 'action' => 'verify',$activationKey), true);
					$url1 = 'http://13.58.29.166/phishing/#/verify-email/'.$activationKey;
                    $url = "<a href='$url1'>Click Here</a>";			
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
					$email_template = str_replace('[link]', $url, $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    //$email_template = str_replace('[password]', $pass, $email_template);
					$subject = 'Registration  Email';
					$sender_email = 'info@phishing.com';
					 /*$this->sendEmail($email, $sender_email, $subject, 'common', $email_template);
					 $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'User Created Successfully');
					 return json_encode($arr);*/
					try{
                             $this->sendEmail($email, $sender_email, $subject, 'common', $email_template);
                             $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'User Created Successfully');
					         return json_encode($arr);
                      }catch(Exception $e){
                            $msg='Something went wrong! Please try again.';
							$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
							return json_encode($arr);
                        }
					
				}else{
					$msg='Something went wrong! Please try again.';
					$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					return json_encode($arr);
					}
			}	
        }
    }
	
	 public function forgot_password() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['email'];
            if (!empty($email)) {
                $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'forgot_password')));

                $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
                $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
                $subject = $email_template_detail['EmailTemplate']['subject'];
                $sender_name = $email_template_detail['EmailTemplate']['sender_name'];

                $Userr = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if (!empty($Userr)) {
                    $newpassword = $this->__generatePassword();
                    $Userr['User']['password'] = $this->Auth->password($newpassword);
                    $Userr['User']['id'];

                    $name = $Userr['User']['name'];
                    $email = $Userr['User']['email'];
                    $pass = $newpassword;
					
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[fullname]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    $email_template = str_replace('[PASSWORD]', $pass, $email_template);
					
					//try{
                            $this->sendEmail($this->request->data['email'], $sender_email, $subject, 'common', $email_template);
                             if ($this->User->Save($Userr['User'])) {
								$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Please check your email for reset password.'); 
		    					echo json_encode($arr); die; 
							}else{	
								$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'unable to send email.'); 
		    					echo json_encode($arr); die;
							}
                        /*}catch(Exception $e){
                            $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'unable to send email.'.$e); 
		    				echo json_encode($arr); die;
                        }*/
						
                } else {
                    $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'No user found.'); 
		    		echo json_encode($arr); die; 
                }
            } else {
                $arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'No email found.'); 
		    	echo json_encode($arr); die;
            }
        }
    }
	
	public function client_list() {
        $this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			$result=array();
			$client = $this->Client->find('all',array('conditions'=>array('Client.user_id'=>$user_id),'order'=>'Client.id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client List',"data"=>$client); 
		    echo json_encode($arr); die; 
        }		
    }
	
	public function client_add() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			// $client_name = $this->request->data['client_name'];
			// $industry = $this->request->data['industry'];
			// $official_website = $this->request->data['official_website'];
			$domain_name = $this->request->data['domain_name'];
			if(!empty($this->request->data['id'])){
				$this->request->data['Client']['id'] = $this->request->data['id'];
			}
			$this->request->data['Client']['user_id']=$user_id;
			$this->request->data['Client']['client_name']=$this->request->data['client_name'];
			$this->request->data['Client']['industry'] = $this->request->data['industry'];
			$this->request->data['Client']['official_website']= $this->request->data['official_website'];
			$this->request->data['Client']['domain_name']= $this->request->data['domain_name'];

			$this->request->data['Client']['country']= $this->request->data['country'];
			$this->request->data['Client']['facebook']= !empty($this->request->data['facebook'])?$this->request->data['facebook']:'';
			$this->request->data['Client']['twitter']= !empty($this->request->data['twitter'])?$this->request->data['twitter']:'';
			$this->request->data['Client']['linkedin']= !empty($this->request->data['linkedin'])?$this->request->data['linkedin']:'';

			$clientData = $this->Client->find('count',array('conditions'=>array('Client.client_name'=>$this->request->data['client_name'])));
			// if($clientData >= 1 && empty($this->request->data['id'])){
			// 	 $msg='This Client already exist.';
			// 	 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
			// 	 return json_encode($arr);
			// }else{ 
				if ($this->Client->save($this->request->data, false)) {
					$clientid = $this->Client->getLastInsertID();
					//$clientData  = $this->Client->findById($clientid);
					if(empty($this->request->data['id'])){
						if(exec("/usr/bin/python /var/www/html/python/dnstwist.py ".$domain_name."___".$clientid)){
							exec("/usr/bin/python /var/www/html/python/punicoder1.py ".$domain_name." ".$clientid);
							exec("/usr/bin/python3 /var/www/html/python/gitgot.py ".$domain_name."_".$clientid);
							$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
						}else{
							$arr = array("statusCode" => "400", "replyStatus" => "success", "replyMessage" =>'Client Added Successfully, but python not executed');
						}
						$this->check_reported_to_google($clientid);
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
					}else{
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
					}
					
					return json_encode($arr);
				}	
			// }	
        }
	}
	
	public function get_appinfo($client_id){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$result=array();
			$client = $this->ClientAppinfo->find('all',array('conditions'=>array('client_id'=>$client_id),'order'=>'id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client app info List',"data"=>$client); 
		    echo json_encode($arr); die; 
        }
	}

	public function add_appinfo(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$app_name  = $this->request->data['app_name'];
			// $package_name = $this->request->data['package_name'];
			// $corrupted  = $this->request->data['corrupted'];
			$sha256 = $this->request->data['sha256'];
			$this->request->data['ClientAppinfo']['client_id']=$client_id;
			$this->request->data['ClientAppinfo']['app_name']= $app_name;
			// $this->request->data['ClientAppinfo']['package_name'] = $package_name;
			$this->request->data['ClientAppinfo']['version'] = $this->request->data['version'];
			$this->request->data['ClientAppinfo']['sha256']= $sha256!=''?$sha256:'';
			// $this->request->data['ClientAppinfo']['corrupted']= $corrupted==True?'Yes':'No';
			if ($this->ClientAppinfo->save($this->request->data, false)) {
				$lastInsertId = $this->ClientAppinfo->getLastInsertId();
				// exec("/usr/bin/python /var/www/html/python/analysis.py ".$sha256." ".$lastInsertId);
				if(exec("/usr/bin/python /var/www/html/python/analysis.py ".$sha256." ".$lastInsertId)){
					// $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
				}else{
					// $arr = array("statusCode" => "400", "replyStatus" => "success", "replyMessage" =>'Client Added Successfully, but python not executed');
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client app Info Added Successfully');
				return json_encode($arr);
			}		
        }
	}

	public function update_appinfo($id){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$package_name = $this->request->data['package_name'];
			$corrupted  = $this->request->data['corrupted'];	
			$this->request->data['ClientAppinfo']['id']=$id;
			$this->request->data['ClientAppinfo']['package_name'] = $package_name;
			$this->request->data['ClientAppinfo']['corrupted']= $corrupted==True?'Yes':'No';
			$this->ClientAppinfo->save($this->request->data, false);
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');			
			return json_encode($arr);
		}
	}

	public function get_people($client_id){
		$this->layout = false;
		$this->autoRender = false;
		$this->People->bindModel(
			array('hasMany'=>array(
				'TwitterData' => array(
					'className' => 'TwitterData',
					'foreignKey' => 'people_id',
					'dependent' => true
					// 'conditions' => array(
					// 	'VirusTotal.id = VirusTotalScan.virus_total_scan_id',
					// )
					)
		)));
		$this->People->bindModel(
			array('hasMany'=>array(
				'InstagramData' => array(
					'className' => 'InstagramData',
					'foreignKey' => 'people_id',
					'dependent' => true
					)
		)));
		if ($this->request->is('post') || $this->request->is('get')) {
			$result=array();
			$people = $this->People->find('all',array('conditions'=>array('client_id'=>$client_id),'order'=>'id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client people List',"data"=>$people); 
		    echo json_encode($arr); die; 
        }
	}

	public function add_people(){
		$this->layout = false;
		$this->autoRender = false;
		// $command = escapeshellcmd("/usr/bin/python /var/www/html/python/twitter-data.py svnlabs 10");
		
		// if(shell_exec(shell_exec($command))){
		// 	$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
		// }else{
		// 	$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Client Added Successfully, but python not executed');
		// }
		// return json_encode($arr);
        if ($this->request->is('post') || $this->request->is('put')) {
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}
			$pastebin = !empty($this->request->data['pastebin'])?$this->request->data['pastebin']:'';
			$this->request->data['People']['client_id']= $this->request->data['client_id'];
			$this->request->data['People']['first_name']= $this->request->data['first_name'];
			$this->request->data['People']['last_name']= !empty($this->request->data['last_name'])?$this->request->data['last_name']:'';
			$this->request->data['People']['facebook_profile'] = !empty($this->request->data['facebook_profile'])?$this->request->data['facebook_profile']:'';
			$this->request->data['People']['twitter_profile'] = !empty($this->request->data['twitter_profile'])?$this->request->data['twitter_profile']:'';
			$this->request->data['People']['linkedin_profile']= !empty($this->request->data['linkedin_profile'])?$this->request->data['linkedin_profile']:'';
			$this->request->data['People']['instagram_profile']= !empty($this->request->data['instagram_profile'])?$this->request->data['instagram_profile']:'';
			$this->request->data['People']['pastebin']= $pastebin;
			if ($this->People->save($this->request->data, false)) {
				$people_id = $this->People->getLastInsertId();;
				if(exec("/usr/bin/python /var/www/html/python/twitter-data.py ".$this->request->data['People']['twitter_profile']." ".$people_id)){
					exec("/usr/bin/python3 /var/www/html/python/insta.py ".$this->request->data['People']['instagram_profile']." ".$people_id);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
				}else{
					$arr = array("statusCode" => "201", "replyStatus" => "/usr/bin/python /var/www/html/python/pwnbin.py ".$pastebin." ".$client_id, "replyMessage" =>'Client Added Successfully, but python not executed');
				}
				// $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client people Added Successfully');
				return json_encode($arr);
			}		
		}
	}

	public function get_apkinfo($client_id){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$result=array();
			$client = $this->ClientApk->find('all',array('conditions'=>array('client_id'=>$client_id),'order'=>'id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client app info List',"data"=>$client); 
		    return json_encode($arr); die; 
        }
	}

	public function add_apk_info(){
		$this->layout = false;
		$this->autoRender = false;
		// $arr = array("statusCode" => "200", "replyStatus" => "success", "data" =>$this->request->data);
		// return json_encode($arr);
        if ($this->request->is('post') || $this->request->is('put')) {
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}

			// $filename = $this->request->data['apk']['name'];
			$filename = time().".apk";
			$this->request->data['ClientApk']['client_id']=$client_id;
			$this->request->data['ClientApk']['apk']= $filename;
			$this->request->data['ClientApk']['link'] = $this->request->data['link'];

			$filepath = '../../'.$filename; 
			// move_uploaded_file($this->request->data['apk'],$filepath);
		   	if(move_uploaded_file($this->request->data['apk'],$filepath)){   
				if ($this->ClientApk->save($this->request->data, false)) {
					$lastInsertId = $this->ClientApk->getLastInsertId();
					if(exec("/usr/bin/python /var/www/html/python/file_to_sha256.py ".$filename." ".$lastInsertId)){
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
					}else{
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added Successfully, but python not executed');
					}
					// $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'ClientData Added Successfully','data'=>$this->request->data);
					return json_encode($arr);
				}
			}else{
				$arr = array("statusCode" => "400", "replyStatus" => "erroe", "replyMessage" =>'File not uploaded','data'=>$this->request->data);
				return json_encode($arr);		
			}

        }
	}

	public function apk_upload(){
		$this->layout = false;
        $this->autoRender = false;
		$arr = array("statusCode" => "200", "replyStatus" => "error", "data1" =>$_FILES,'data'=>$this->request->data);
		return json_encode($arr);die;
		$filename = $this->request->data['name'];
		$value = $this->request->data['vlaue'];
		// $filepath = '../../'.$filename; 
		// $arr = array("statusCode" => "200", "replyStatus" => "error", "replyMessage" =>'File upload failed.','data'=>$this->request->file);
		// return json_encode($arr);die;
		if ($this->request->is('post') || $this->request->is('put')) {
			// if(move_uploaded_file($this->request->data['uploadFile']['tmp_name'],$filepath)){
			// 	$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'File uploaded Successfully.');
			// }else{
			// 	$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" =>'File upload failed.');
			// }
			// $arr = array("statusCode" => "200", "replyStatus" => "error", "replyMessage" =>'File upload failed.','data'=>$this->request->data);
			// return json_encode($arr);
			if(file_put_contents($filename, base64_decode($value))){
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'File uploaded Successfully.');
			}else{
				$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" =>'File upload failed.');
			}
			return json_encode($arr);
		}
	}

	public function update_apkinfo($id){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['ClientApk']['id']=$id;
			$this->request->data['ClientApp']['sha256'] = $this->request->data['sha256'];
			$this->ClientAppinfo->save($this->request->data, false);
						
        }
	}

	public function add_phishing_report(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			
			$sid=$this->request->header('sid');
			$user_id = base64_decode($sid);
			$settings = $this->Setting->find('first',array('conditions'=>array('user_id'=>$user_id)));
			
			$this->request->data['PhishingReport']['client_id']=$this->request->data['client_id'];
			$this->request->data['PhishingReport']['dnstwist_id'] = $this->request->data['dnstwist_id'];
			$this->request->data['PhishingReport']['url']= $this->request->data['url'];
			// $this->request->data['PhishingReport']['report_to']= $this->request->data['report_to'];
			// $this->request->data['PhishingReport']['status']= 0;
			$this->request->data['PhishingReport']['google']= !empty($this->request->data['google'])?$this->request->data['google']:0;
			$this->request->data['PhishingReport']['microsoft']= !empty($this->request->data['microsoft'])?$this->request->data['microsoft']:0;
			$this->request->data['PhishingReport']['phishtank']= !empty($this->request->data['phishtank'])?$this->request->data['phishtank']:0;
			$this->request->data['PhishingReport']['uscart']= !empty($this->request->data['uscart'])?$this->request->data['uscart']:0;
			$this->request->data['PhishingReport']['antiphishing']= !empty($this->request->data['antiphishing'])?$this->request->data['antiphishing']:0;
			$this->request->data['PhishingReport']['netcraft']= !empty($this->request->data['netcraft'])?$this->request->data['netcraft']:0;
			$this->request->data['PhishingReport']['message']= $this->request->data['message']?$this->request->data['message']:0;
			$email = $this->request->data['to'];
			if($this->PhishingReport->save($this->request->data, false)){

			
				$smtp['host'] = $settings['Setting']['smtp_host'];
				$smtp['port'] = $settings['Setting']['smtp_port'];
				$smtp['email'] = $settings['Setting']['smtp_email'];
				$smtp['password'] = $settings['Setting']['smtp_password'];
				
				$email_template = str_replace('[domain_name]', $this->request->data['PhishingReport']['url'], $this->request->data['PhishingReport']['message']);
				$subject = 'Report Phishing';
				$sender_email = 'info@phishing.com';
				try{
					$this->sendEmail($email, $sender_email, $subject, 'common', $email_template,null,'',$smtp);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Report mailed Successfully');
					return json_encode($arr);
				}catch(Exception $e){
					$msg='Something went wrong! Please try again.';
					$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					return json_encode($arr);
				}
			}
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'ClientData Added Successfully');
			return json_encode($arr);				
        }
	}

	public function get_phishing_report($client_id){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$result=array();
			$reports = $this->PhishingReport->find('all',array('conditions'=>array('client_id'=>$client_id),'order'=>'id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Phishing Report List',"data"=>$reports); 
		    echo json_encode($arr); die; 
        }
	}

	
	
	public function recently_added_domain($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $domains = $this->DomainInfo->find('all',array('conditions'=>array('DomainInfo.client_id'=>$client_id),'limit' => 14,'order'=>'DomainInfo.id desc'));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Info List',"data"=>$domains); 
		echo json_encode($arr); die;  		
	}

	
	public function scan_result(){
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->Dnstwist->bindModel(
			array('hasOne'=>array(
				'VirusTotal' => array(
					'className' => 'VirusTotal',
					'foreignKey' => false,
					'conditions' => array(
						'Dnstwist.domain_name = VirusTotal.resource',
					))
		)));

		$this->Dnstwist->bindModel(
			array('hasOne'=>array(
				'DomainInfo' => array(
					'className' => 'DomainInfo',
					'foreignKey' => false,
					'conditions' => array(
						'Dnstwist.domain_name = DomainInfo.domain_name',
					))
		)));
        //$dnstwist = $this->Dnstwist->find('all',array('conditions'=>array('Dnstwist.client_id'=>$client_id),'limit'=>10,'order'=>'Dnstwist.id DESC'));
		$conditions = array();
		if ($this->request->is('post') || $this->request->is('put')) {
			 $client_id = $this->request->data['client_id'];
			 $limit = !empty($this->request->data['limit'])?$this->request->data['limit']:10;
			 $conditions[] = array('Dnstwist.client_id'=>$client_id,'Dnstwist.dns_a !='=>'');
		}
		if($limit==500){
			$dnstwist = $this->Dnstwist->find('all',array('conditions'=>$conditions));
		}else{
			$this->paginate = array('conditions'=>$conditions, 'limit' => $limit,'order'=>'Dnstwist.id asc');
        	$dnstwist = $this->paginate('Dnstwist');
		}
		
		//echo "<pre>";
		//print_r($dnstwist);die;
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist List',"data"=>$dnstwist); 
		echo json_encode($arr); die;  		
	}
	
	public function impersonated_domain_locations($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->DomainInfo->bindModel(array('hasOne'=>array(
		'DomainRegistrantContact' => array(
            'className' => 'DomainRegistrantContact',
            'foreignKey' => false,
            'conditions' => array(
            'DomainInfo.id = DomainRegistrantContact.domain_id',
            )))));
        $domainInfo = $this->DomainInfo->find('all',array('fields'=>array('DomainRegistrantContact.country_name','Count(DomainRegistrantContact.id) as count'),'conditions'=>array('DomainInfo.client_id'=>$client_id)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'DomainInfo List',"data"=>$domainInfo); 
		echo json_encode($arr); die;  		
	}

	
	
	public function dashboard_count($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        // $impersonatedSites = $this->DomainInfo->find('count',array('conditions'=>array('DomainInfo.client_id'=>$client_id)));
        $impersonatedSites = $this->Dnstwist->find('count',array('conditions'=>array('Dnstwist.client_id'=>$client_id)));
        $infectedSites = $this->VirusTotal->find('count',array('conditions'=>array('VirusTotal.positives !='=>0,'VirusTotal.client_id'=>$client_id)));
        $unregisterSites = $this->Dnstwist->find('count',array('conditions'=>array('Dnstwist.client_id'=>$client_id,'Dnstwist.dns_a'=>'')));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Info List',"impersonatedSites"=>$impersonatedSites,"infectedSites"=>$infectedSites,"unregisterSites"=>$unregisterSites); 
		echo json_encode($arr); die;  		
	}
	
	public function add_domain_info($client_id= null) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$domain_name = $this->request->data['domain_name'];
			$query_time = $this->request->data['query_time'];
			$whois_server = $this->request->data['whois_server'];
			$domain_registered = $this->request->data['domain_registered'];
			$create_date = $this->request->data['create_date'];
			$update_date = $this->request->data['update_date'];
			$expiry_date = $this->request->data['expiry_date'];
			$status = $this->request->data['status'];
			
			$name_servers ='';
			$domain_status = '';
			$api_credits_charged = '';
			$whois_retrieval_time = '';
			$user_ip_address = '';
			if(!empty($this->request->data['name_servers'])){
				$name_servers = implode(",",$this->request->data['name_servers']);
			}
            if(!empty($this->request->data['domain_status'])){
				$domain_status = implode(",",$this->request->data['domain_status']);
			}
			if(!empty($this->request->data['query_stats']['api_credits_charged'])){
               $api_credits_charged = $this->request->data['query_stats']['api_credits_charged'];
			}   
			if(!empty($this->request->data['query_stats']['whois_retrieval_time'])){
               $whois_retrieval_time = $this->request->data['query_stats']['whois_retrieval_time'];
			}   
			if(!empty($this->request->data['query_stats']['user_ip_address'])){
               $user_ip_address = $this->request->data['query_stats']['user_ip_address'];
			}   
			$this->request->data['DomainInfo']['client_id']=$client_id;
			$this->request->data['DomainInfo']['domain_name']=$domain_name;
			$this->request->data['DomainInfo']['query_time']=$query_time;
			$this->request->data['DomainInfo']['whois_server'] = $whois_server;
			$this->request->data['DomainInfo']['domain_registered']= $domain_registered;
			$this->request->data['DomainInfo']['create_date']= $create_date;
			$this->request->data['DomainInfo']['update_date']= $update_date;
			$this->request->data['DomainInfo']['expiry_date']= $expiry_date;
			$this->request->data['DomainInfo']['status']= $status;
			$this->request->data['DomainInfo']['name_servers']= $name_servers;
			$this->request->data['DomainInfo']['domain_status']= $domain_status;
			$this->request->data['DomainInfo']['api_credits_charged']= $api_credits_charged;
			$this->request->data['DomainInfo']['whois_retrieval_time']= $whois_retrieval_time;
			$this->request->data['DomainInfo']['user_ip_address']= $user_ip_address;
			$domainData = $this->DomainInfo->find('count',array('conditions'=>array('DomainInfo.domain_name'=>$domain_name)));
			// if($domainData >= 1){
			// 	 $msg='This domain name allready exist.';
			// 	 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
			// 	 return json_encode($arr);
			// }else{ 
				if ($this->DomainInfo->save($this->request->data, false)) {
					$lastInsertId = $this->DomainInfo->getLastInsertId();
					if($this->request->data['domain_registrar']) $this->add_domain_registrar($this->request->data['domain_registrar'],$lastInsertId);
					if($this->request->data['registrant_contact']) $this->add_domain_registrant_contact($this->request->data['registrant_contact'],$lastInsertId);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Added Successfully');
					return json_encode($arr);
				}	
			//}	
        }
    }
	
	public function add_domain_registrar($data,$domain_id) {
        $this->layout = false;
        $this->autoRender = false;	
        // if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $domain_id = $data['domain_id'];
			$iana_id = $data['iana_id'];
			$registrar_name = $data['registrar_name'];
			$whois_server = $data['whois_server'];
			$website_url = $data['website_url'];
			$email_address = $data['email_address'];
			$phone_number = $data['phone_number'];

			$this->request->data['DomainRegistrar']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrar']['iana_id']=$iana_id;
			$this->request->data['DomainRegistrar']['registrar_name'] = $registrar_name;
			$this->request->data['DomainRegistrar']['whois_server']= $whois_server;
			$this->request->data['DomainRegistrar']['website_url']= $website_url;
			$this->request->data['DomainRegistrar']['email_address']= $email_address;
			$this->request->data['DomainRegistrar']['phone_number']= $phone_number;
			if ($this->DomainRegistrar->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrar Successfully');
				return json_encode($arr);
			}		
        // }
    }
	
	public function add_domain_registrant_contact($data,$domain_id) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $domain_id = $this->request->data['domain_id'];
			$full_name = $data['full_name'];
			$company_name = $data['company_name'];
			$mailing_address = $data['mailing_address'];
			$city_name = $data['city_name'];
			$state_name = $data['state_name'];
			$zip_code = $data['zip_code'];
			$country_name = $data['country_name'];
			$country_code = $data['country_code'];
			$email_address = $data['email_address'];
			$phone_number = $data['phone_number'];
			$fax_number = $data['fax_number'];
			$this->request->data['DomainRegistrantContact']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrantContact']['full_name']=$full_name;
			$this->request->data['DomainRegistrantContact']['company_name'] = $company_name;
			$this->request->data['DomainRegistrantContact']['mailing_address']= $mailing_address;
			$this->request->data['DomainRegistrantContact']['city_name']= $city_name;
			$this->request->data['DomainRegistrantContact']['state_name']= $state_name;
			$this->request->data['DomainRegistrantContact']['zip_code']= $zip_code;
			$this->request->data['DomainRegistrantContact']['country_name']= $country_name;
			$this->request->data['DomainRegistrantContact']['country_code']= $country_code;
			$this->request->data['DomainRegistrantContact']['email_address']= $email_address;
			$this->request->data['DomainRegistrantContact']['phone_number']= $phone_number;
			$this->request->data['DomainRegistrantContact']['fax_number']= $fax_number;
			if ($this->DomainRegistrantContact->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrant Contact Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	
	public function add_catch_phishing() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			
			$score = $this->request->data['score'];

			if($score>=90)
				$status = 'Suspicious';
			elseif($score>=80)
				$status = 'Likely';
			elseif($score>=65)
				$status = 'Potential';
			
			
			$domain = $this->request->data['domain'];
			$this->request->data['CatchPhishing']['status']=$status;
			$this->request->data['CatchPhishing']['score']=$score;
			$this->request->data['CatchPhishing']['domain'] = $domain;
			if ($this->CatchPhishing->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Catch Phising Added Successfully.');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_catch_phishing_company() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
				
			$score = $this->request->data['score'];
			if($score>=90)
				$status = 'Suspicious';
			elseif($score>=80)
				$status = 'Likely';
			elseif($score>=65)
				$status = 'Potential';
			$domain = $this->request->data['domain'];
			$company = $this->request->data['company'];

			$this->request->data['CatchPhishingCompany']['status']=$status;
			$this->request->data['CatchPhishingCompany']['score']=$score;
			$this->request->data['CatchPhishingCompany']['domain'] = $domain;
			$this->request->data['CatchPhishingCompany']['company'] = $company;
			if ($this->CatchPhishingCompany->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Catch Phising Added Successfully.');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_virus_total_info($client_id = null) {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			// $client_id =1;
			// if(!empty($this->request->data['client_id'])){
			// 	$client_id = $this->request->data['client_id'];
			// }	
			$scan_id = $this->request->data['scan_id'];
			$resource = $this->request->data['resource'];
			$url  = $this->request->data['url'];
			$scan_date = $this->request->data['scan_date'];
			$permalink  = $this->request->data['permalink'];
			$positives = $this->request->data['positives'];
			$total = $this->request->data['total'];
			$scans = $this->request->data['scans'];
			$this->request->data['VirusTotal']['client_id']=$client_id;
			$this->request->data['VirusTotal']['scan_id']=$scan_id;
			$this->request->data['VirusTotal']['resource'] = $resource;
			$this->request->data['VirusTotal']['url']= $url;
			$this->request->data['VirusTotal']['scan_date']= $scan_date;
			$this->request->data['VirusTotal']['permalink']= $permalink;
			$this->request->data['VirusTotal']['positives']= $positives;
			$this->request->data['VirusTotal']['total']= $total;
			// $this->request->data['VirusTotal']['scans']= $scans;
			if ($this->VirusTotal->save($this->request->data, false)) {
				$lastInsertId = $this->VirusTotal->getLastInsertId();
				$this->add_virus_total_scan($this->request->data['scans'],$lastInsertId);
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Virus total scan Added Successfully');
				return json_encode($arr);
			}		
        }
	}
	
	public function add_virus_total_scan($data1,$id){
		$this->layout = false;
        $this->autoRender = false;
		$this->request->data['VirusTotalScan']['virus_total_scan_id'] = $id;
        $keys = array_keys($data1);
        $count=0;
        foreach($data1 as $key){
			$this->VirusTotalScan->create(false);
			$this->request->data['VirusTotalScan']['site'] = $keys[$count];
			$this->request->data['VirusTotalScan']['detected'] = $key['detected']?'TRUE':'FALSE';
			$this->request->data['VirusTotalScan']['result'] = $key['result'];
			$this->VirusTotalScan->saveAll($this->request->data['VirusTotalScan']);
            $count++;
        } 
	}
	
	/*public function add_virus_total_scan_info() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$virus_total_scan_id =1;
			if(!empty($this->request->data['virus_total_scan_id'])){
				$virus_total_scan_id = $this->request->data['virus_total_scan_id'];
			}	
			$site = $this->request->data['site'];
			$detected = $this->request->data['detected'];
			$result = $this->request->data['result'];
			$this->request->data['VirusTotalScan']['virus_total_scan_id']=$virus_total_scan_id;
			$this->request->data['VirusTotalScan']['site']=$site;
			$this->request->data['VirusTotalScan']['detected'] = $detected;
			$this->request->data['VirusTotalScan']['result']= $result;
			if ($this->VirusTotalScan->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Virus Total Scan Info Added Successfully.');
				return json_encode($arr);
			}		
        }
    } */
	
	
	public function add_dns_twist() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$fuzzer  = $this->request->data['fuzzer'];
			$domain_name = $this->request->data['domain-name'];
			$dns_a  = $this->request->data['dns-a']?implode(",",$this->request->data['dns-a']):'';
			$this->request->data['Dnstwist']['client_id']=$client_id;
			$this->request->data['Dnstwist']['fuzzer']=$fuzzer;
			$this->request->data['Dnstwist']['domain_name'] = $domain_name;
			$this->request->data['Dnstwist']['dns_a']= $dns_a;
			if ($this->Dnstwist->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	
	public function graphApi() {
		$this->layout = false;
        $this->autoRender = false;	
		if ($this->request->is('post') || $this->request->is('put')) {
			$client_id = $this->request->data['client_id'];
			$mode = $this->request->data['mode'];
			//$impersonatedSites = $this->Dnstwist->find('count',array('conditions'=>array('Dnstwist.client_id'=>$client_id)));	
			$conditions = array('Dnstwist.client_id'=>$client_id);
			$monthUsersCount=array();
			$usersList = array();

			$this->Dnstwist->bindModel(
				array('hasOne'=>array(
					'DomainInfo' => array(
						'className' => 'DomainInfo',
						'foreignKey' => false,
						'conditions' => array(
							'Dnstwist.domain_name = DomainInfo.domain_name',
						))
			)));
			if($mode =='monthly'){
				$year = $this->request->data['year'];
				$conditions = array('Dnstwist.client_id'=>$client_id,'YEAR(DomainInfo.create_date)'=>$year);

			//    $usersbymonth = $this->Dnstwist->find('all',array('conditions'=>array('YEAR(Dnstwist.created)' => date('Y'), $conditions ), 'group'=>array('MONTH(Dnstwist.created)'), 'fields'=>array('Count(Dnstwist.id) as count', 'MONTH(Dnstwist.created) as month'), 'order'=>array('Dnstwist.created'=>'ASC')));
			   $usersbymonth = $this->Dnstwist->find('all',
								   array(
									   	   'conditions'=>array($conditions ), 
										   'group'=>array('MONTH(DomainInfo.create_date)'), 
										   'fields'=>array('Count(DomainInfo.id) as count', 'MONTH(DomainInfo.create_date) as month'), 
										   'order'=>array('Dnstwist.created'=>'ASC')
										)
									);
			   //$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                //echo "<pre>";
                //print_r($log);die;
				if(!empty($usersbymonth)){
					$fullMonthList = array(1=>'Jan',2=>'Feb',3=>'March',4=>'April',5=>'may',6=>'june',7=>'July',8=>'august',9=>'September',10=>'October',11=>'November',12=>'December');
					foreach($usersbymonth as $usersbymonths){
						$monthUsersCount[$usersbymonths[0]['month']] = $usersbymonths[0]['count'];
					}
					foreach($fullMonthList as $key=>$vel){
						//if($key <= date('m')){
							if(in_array($key, array_keys($monthUsersCount))){
								$usersList[] = $monthUsersCount[$key];
							}else{
								$usersList[] = 0;
							}
						//}
					}
					//$usersList = implode(',', $usersList); 
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
			}elseif($mode =='weekly'){	
			    $weekUsersCount=array();
				$usersbyweek = $this->Dnstwist->find('all',array('conditions'=>array('MONTH(Dnstwist.created)' => date('m'), $conditions ),'group'=>array('WEEK(Dnstwist.created)'), 'fields'=>array('Count(Dnstwist.id) as count', 'WEEK(Dnstwist.created) as week'), 'order'=>array('Dnstwist.created'=>'ASC')));
				//$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                //echo "<pre>";
                //print_r($log);die;
				$fullWeekList = array(1=>'First',2=>'Second',3=>'Third',4=>'Fourth',5=>'Fifth');
				foreach($usersbyweek as $usersbyweeks){
					$usersbyweeks[0]['week'] = $usersbyweeks[0]['week'] % 7;
					$weekUsersCount[$usersbyweeks[0]['week']] = $usersbyweeks[0]['count'];
				}
				foreach($fullWeekList as $key=>$vel){
					//if($key <= date('m')){
						if(in_array($key, array_keys($weekUsersCount))){
							$usersList[] = $weekUsersCount[$key];
						}else{
							$usersList[] = 0;
						}
					//}
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
				//echo "<pre>";
				//print_r($usersList);
				//die;
			}else{
				$yearUsersCount=array();
				$fullYearList = array();
				$currentyear = date('Y');
				$last10year = date('Y') - 20;
				$usersbyyear = $this->Dnstwist->find('all',
								array(
									// 'conditions'=>array('YEAR(DomainInfo.create_date) >' => $last10year, $conditions ),
									'conditions'=>array($conditions ),
									'group'=>array('YEAR(DomainInfo.create_date)'), 
									'fields'=>array('Count(DomainInfo.id) as count', 'YEAR(DomainInfo.create_date) as year','YEAR(MIN(DomainInfo.create_date)) as minyear','YEAR(MAX(DomainInfo.create_date)) as maxyear'), 
									'order'=>array('DomainInfo.create_date'=>'ASC')
								)
							);
			     //$log = $this->Dnstwist->getDataSource()->getLog(false, false);
                 //echo "<pre>";
                 //print_r($log);die;
				 for($i=$last10year+1; $i <= $currentyear; $i++){
					 $fullYearList[$i] = $i;
				 }	 
				 //echo "<pre>";
				 //print_r($usersbyyear);die;
                 //print_r($fullYearList);die;
				foreach($usersbyyear as $usersbyyears){
					$yearUsersCount[$usersbyyears[0]['year']] = $usersbyyears[0]['count'];
				}
				//echo "<pre>";
                //print_r($yearUsersCount);die;
				foreach($fullYearList as $key=>$vel){
					//if($key <= date('m')){
						if(in_array($key, array_keys($yearUsersCount))){
							$usersList[] = $yearUsersCount[$key];
						}else{
							$usersList[] = 0;
						}
					//}
				}
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist count','mode'=>$mode, 'data'=>$usersList);
				return json_encode($arr);
			}				
		}			
	}
	
	public function get_whois_data($domain_id){
		$this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->DomainInfo->bindModel(
			array('hasOne'=>array(
				'DomainRegistrantContact' => array(
					'className' => 'DomainRegistrantContact',
					'foreignKey' => false,
					'conditions' => array(
						'DomainInfo.id = DomainRegistrantContact.domain_id',
					))
		)));

		$this->DomainInfo->bindModel(
			array('hasOne'=>array(
				'DomainRegistrar' => array(
					'className' => 'DomainRegistrar',
					'foreignKey' => false,
					'conditions' => array(
						'DomainInfo.id = DomainRegistrar.domain_id',
					))
			))
		);
        $domainInfo = $this->DomainInfo->find('first',array('conditions'=>array('DomainInfo.id'=>$domain_id)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'DomainInfo List',"data"=>$domainInfo); 
		echo json_encode($arr); die; 
	}


	public function get_virus_total_data($resource){
		$this->layout = false;
        $this->autoRender = false;
		$result=array();
		$this->VirusTotal->bindModel(
			array('hasMany'=>array(
				'VirusTotalScan' => array(
					'className' => 'VirusTotalScan',
					'foreignKey' => 'virus_total_scan_id',
					'dependent' => true
					// 'conditions' => array(
					// 	'VirusTotal.id = VirusTotalScan.virus_total_scan_id',
					// )
					)
		)));

		
        $virusTotal = $this->VirusTotal->find('first',array('conditions'=>array('VirusTotal.resource'=>$resource)));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'VirusTotal List',"data"=>$virusTotal); 
		echo json_encode($arr); die; 
	}
	
	public function logout() {
        $this->Session->setFlash('Logout Successfully.','success');
        $this->Auth->logout();
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

   public function verify_email($userid){
		$this->layout = false;
		$this->autoRender = false;
        $userId = base64_decode($userid);
        $userData['User']['id'] = $userId;
        $userData['User']['verified'] = 1;
        $checkuser = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
        if($checkuser){
            if ($this->User->save($userData['User'], false)) {
                $sendarray = array("statusCode"=>200,"replyMessage" => "User verified successfully.","usre_id"=>$userid);
            }else{
                $sendarray = array("statusCode"=>201,"replyMessage" => "User not verified successfully.");
            }
        }else{
            $sendarray = array("statusCode"=>201,"replyMessage" => "User not exist.");
        }
         
        echo json_encode($sendarray);
    }
	
	
	
   
	
  public function getToken($length){
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(50);
        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        return $token;
   }

   public function setLatLng(){
		$this->layout = false;
		$this->autoRender = false;
		$domainAddress = $this->DomainRegistrantContact->find('all',array('fields'=>array('mailing_address','city_name','state_name','country_name'),'conditions'));
		
		for($i=0;$i<count($domainAddress);$i++){
			if($i<=1){
				$address = $domainAddress[$i]['DomainRegistrantContact']['mailing_address'].",".$domainAddress[$i]['DomainRegistrantContact']['city_name'].",".$domainAddress[$i]['DomainRegistrantContact']['state_name'].",".$domainAddress[$i]['DomainRegistrantContact']['country_name'];
				$result = $this->get_lat_long($address);
				echo "coming====";
				print($result);
			}	
		}
		
	}

	// function to get  the address
	public function get_lat_long($address){

		$address = str_replace(" ", "+", $address);

		$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&&key=AIzaSyB3-v8GZLBxIq1XHD0nvGAvDDqUKUriIWI");
		$json = json_decode($json);
		echo "coming";
		print_r($json);
		$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
		$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		return $lat.','.$long;
	}

	public function update_setting(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->request->data['id'])){
				$this->request->data['Setting']['id']=$this->request->data['id'];
			}
			
			$this->request->data['Setting']['user_id']=$this->request->data['user_id'];
			$this->request->data['Setting']['report_site']=$this->request->data['report_site'];
			// $this->request->data['Setting']['registrar'] = $this->request->data['registrar'];
			$this->request->data['Setting']['smtp_host'] = $this->request->data['smtp_host'];
			$this->request->data['Setting']['smtp_email'] = $this->request->data['smtp_email'];
			$this->request->data['Setting']['smtp_password'] = $this->request->data['smtp_password'];
			$this->request->data['Setting']['smtp_port'] = $this->request->data['smtp_port'];
			// $this->request->data['Setting']['dns_a']= $dns_a;
			if ($this->Setting->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Setting Added Successfully');
				return json_encode($arr);
			}		
        }
	}

	public function get_setting($user_id){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			// $result=array();
			$setting = $this->Setting->find('first',array('conditions'=>array('user_id'=>$user_id)));

			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Setting List',"data"=>$setting); 
		    echo json_encode($arr); die; 
        }
	}

	public function check_reported_to_google($client_id){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$setting = $this->Dnstwist->find('all',array('conditions'=>array('client_id'=>$client_id)));
			for($i=0;$i<count($setting);$i++){
				exec("/usr/bin/python /var/www/html/python/google_safe.py ".$setting[$i]['Dnstwist']['domain_name']." ".$setting[$i]['Dnstwist']['id']);
					
			}
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Setting List'); 
		    echo json_encode($arr); die; 
        }
	}

	public function update_googlesafe($state,$dnstwistid){
		$this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			
				$this->request->data['Dnstwist']['id'] = $dnstwistid;
				$this->request->data['Dnstwist']['reported_to_google'] = $state;
				$this->Dnstwist->save($this->request->data, false);
			
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Setting List'); 
		    echo json_encode($arr); die; 
        }
	}

	public function add_pastebin(){
		
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$this->request->data['Pastebin']['client_id']=$this->request->data['client_id'];
			$this->request->data['Pastebin']['keyword']=$this->request->data['keyword'];
			$this->request->data['Pastebin']['url']=$this->request->data['url'];
			if ($this->Pastebin->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Pastebin url Added Successfully');
				return json_encode($arr);
			}		
        }
	}

	public function add_punicode(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			// $client_id ='';
			// if(!empty($this->request->data['client_id'])){
			// 	$client_id = $this->request->data['client_id'];
			// }	
			$this->request->data['Punicode']['client_id']=$this->request->data['client_id'];
			$this->request->data['Punicode']['domain']=$this->request->data['domain'];
			$this->request->data['Punicode']['result']=$this->request->data['result'];
			if ($this->Punicode->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Punicode  Added Successfully');
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Punicode not added Successfully');	
			}
			return json_encode($arr);		
        }else{
			$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Punicode not added Successfully');	
			return json_encode($arr);
		}
	}

	public function get_punicode_data(){
		$this->layout = false;
		$this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$client_id = $this->request->data['client_id'];
			$this->paginate = array('conditions'=>array('client_id'=>$client_id), 'limit' => 10);
        	$punicodeData = $this->paginate('Punicode');
			$arr = array("data"=>$punicodeData,"statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Punicode List'); 
		    echo json_encode($arr); die; 
        }
		
	}

	public function add_twitter_data(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$client_id ='';
			if(!empty($this->request->data['people_id'])){
				$client_id = $this->request->data['people_id'];
			}	
			$this->request->data['TwitterData']['people_id']=!empty($this->request->data['people_id'])?$this->request->data['people_id']:'';
			$this->request->data['TwitterData']['timeline']=!empty($this->request->data['timeline'])?$this->request->data['timeline']:'';
			$this->request->data['TwitterData']['timeline_created']=!empty($this->request->data['timeline_created'])?$this->request->data['timeline_created']:'';
			$this->request->data['TwitterData']['friend_name']=!empty($this->request->data['friend_name'])?$this->request->data['friend_name']:'';
			$this->request->data['TwitterData']['friend_location']=!empty($this->request->data['friend_location'])?$this->request->data['friend_location']:'';
			if ($this->TwitterData->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Twiter data  Added Successfully');
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Twiter data not added Successfully');	
			}
			return json_encode($arr);		
        }else{
			$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Twiter data not added Successfully');	
			return json_encode($arr);
		}
	}


	public function add_instagram_data(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$people_id = $this->request->data['people_id'];
			$this->request->data['InstagramData']['people_id']=$people_id;
			$this->request->data['InstagramData']['user_id']=!empty($this->request->data['user_id'])?$this->request->data['user_id']:'';
			$this->request->data['InstagramData']['user_name']=!empty($this->request->data['user_name'])?$this->request->data['user_name']:'';
			$this->request->data['InstagramData']['profile_pic_url']=!empty($this->request->data['profile_pic_url'])?$this->request->data['profile_pic_url']:'';
			$this->request->data['InstagramData']['biography']=!empty($this->request->data['biography'])?$this->request->data['biography']:'';
			$this->request->data['InstagramData']['external_url']=!empty($this->request->data['external_url'])?$this->request->data['external_url']:'';
			$this->request->data['InstagramData']['media_count']=!empty($this->request->data['media_count'])?$this->request->data['media_count']:'';
			$this->request->data['InstagramData']['follower_count']=!empty($this->request->data['follower_count'])?$this->request->data['follower_count']:'';
			$this->request->data['InstagramData']['followee_count']=!empty($this->request->data['followee_count'])?$this->request->data['followee_count']:'';
			$this->request->data['InstagramData']['followers']=!empty($this->request->data['followers'])?$this->request->data['followers']:'';
			if ($this->InstagramData->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Twiter data  Added Successfully');
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Twiter data not added Successfully');	
			}
			return json_encode($arr);		
        }else{
			$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Twiter data not added Successfully');	
			return json_encode($arr);
		}
	}

	public function base64toimage($imagecode,$destination) {
       // Image upload /
        if (strpos($imagecode, "png;base64,") != 0)
            $filename = md5(rand()) . ".png";
        else
            $filename = md5(rand()) . ".jpeg";

		$dirpath = $destination;
        $mainImgDirPath = $dirpath . DS . $filename;
        $data_new = $imagecode;
        $pos = strpos($data_new, "base64,");
        if (isset($pos) && $pos != "") {
            $img = substr($data_new, $pos + 7, strlen($data_new));
        } else {
            $img = $data_new;
        }
        $img = str_replace(' ', '+', $img);
        $svdata = base64_decode($img);

        $fp = fopen($mainImgDirPath, 'w');
        if (fwrite($fp, $svdata)) {
            fclose($fp);
        } else {
            $mainImgDirPath = '';
        }
        return $filename;
        // Image upload End /
    }
	

    public function update_site_info(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['Site']['id']=!empty($this->request->data['id'])?$this->request->data['id']:'';
			$this->request->data['Site']['matched']=!empty($this->request->data['matched'])?$this->request->data['matched']:0;
			if ($this->Site->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Site data  Added with image compare Successfully');
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Site data not updated Successfully',"exec"=>"/usr/bin/python /var/www/html/python/scraper.py ".$this->request->data['Site']['fake_site']." ".$file_name." ".$lastInsertId);	
			}
			return json_encode($arr);
				
		}
					
      }

	public function add_site_info(){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {

        	$destination = realpath('/var/www/html/phishing/assets/');
			$file_name = $this->base64toimage($this->request->data['img_file'], $destination);
			if($file_name){
				$this->request->data['Site']['client_id']=!empty($this->request->data['client_id'])?$this->request->data['client_id']:'';
				$this->request->data['Site']['img_file'] = $file_name;
				$this->request->data['Site']['original_site']=!empty($this->request->data['original_site'])?$this->request->data['original_site']:'';
				$this->request->data['Site']['fake_site']=!empty($this->request->data['fake_site'])?$this->request->data['fake_site']:'';
				
				if ($this->Site->save($this->request->data, false)) {
					$lastInsertId = $this->Site->getLastInsertId();
					// if(exec("/usr/bin/python /var/www/html/python/scraper.py ".$this->request->data['Site']['fake_site']." ".$file_name." ".$lastInsertId)){
					exec("/usr/bin/python /var/www/html/python/scraper.py ".$this->request->data['Site']['fake_site']." ".$file_name." ".$lastInsertId);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Site data  Added with image compare Successfully');
					// }else{
					// 	$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Site data not updated Successfully',"exec"=>"/usr/bin/python /var/www/html/python/scraper.py ".$this->request->data['Site']['fake_site']." ".$file_name." ".$lastInsertId);	
					// }
					
				}else{
					$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Site data not added Successfully');	
				}
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Site Image not  uploaded');	
			}
			return json_encode($arr);		
        }
	}

	public function get_sitelist($client_id){
		$this->layout = false;
		$this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$siteData = $this->Site->find('all',array('conditions'=>array('client_id'=>$client_id)));
			$arr = array("data"=>$siteData,"statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client List'); 
		    echo json_encode($arr); die; 
        }
		
	}

public function run_python_github($domain_name,$clientid){
	$this->layout = false;
	$this->autoRender = false;
	// $result = exec("/usr/bin/python /var/www/html/python/img_compare.py");
	// echo $result;
	// $output = shell_exec("python /var/www/script.py")
	// $string = exec("/usr/bin/python /var/www/html/python/dnstwist.py",$output);
	// echo $string;
	// var_dump($output);
	$domain_name = 'http://hdfcbank.com';
	$clientid = 11;
	if(exec("/usr/bin/python /var/www/html/python/scraper.py https://icicibank.com 53233a93b5d47f5664e43ec241814e81.jpeg")){
		$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'github exectued. ');
	}else{
		$arr = array("statusCode" => "400", "replyStatus" => "success", "replyMessage" =>'github not exectued.');
	}
	return json_encode($arr);
}

	public function add_github_result($client_id){
		$this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['GithubData']['client_id']=$client_id;

			if(!empty($this->request->data['result'])){
				$result = $this->request->data['result'];
				$result = str_replace("(u'","'",$result);
				$result = str_replace("u'","'",$result);
				$result = str_replace(")","",$result);
			}
			$this->request->data['GithubData']['result']=$result;
			
			if ($this->GithubData->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Github data  Added Successfully');
			}else{
				$arr = array("statusCode" => "201", "replyStatus" => "error", "replyMessage" =>'Github data not added Successfully');	
			}
			return json_encode($arr);		
        }
	}

	public function get_github_data(){
		$this->layout = false;
		$this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('get')) {
			$client_id = $this->request->data['client_id'];
			$this->paginate = array('conditions'=>array('client_id'=>$client_id), 'limit' => 10);
        	$siteData = $this->paginate('GithubData');
			// $siteData = $this->GithubData->find('all',array('conditions'=>array('client_id'=>$client_id)));
			$arr = array("data"=>$siteData,"statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client List'); 
		    echo json_encode($arr); die; 
        }
		
	}


	
	
	
}
