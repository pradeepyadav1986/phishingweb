<?php

App::uses('Sanitize', 'Utility');

class ApisController extends AppController {

    public $name = 'Users';
    public $uses = array('User','Client', 'DomainInfo','DomainRegistrar','DomainRegistrantContact','VirusTotal','VirusTotalScan','Dnstwist');
    public $helpers = array('Html', 'Form', 'Session','Common');
    public $components = array('Cookie', 'Email','Json','Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login','signup','forgot_password','client_list','client_add','recently_added_domain','scan_result','add_domain_info','add_domain_registrar','add_domain_registrant_contact','add_virus_total_info','add_virus_total_scan', 'add_dns_twist','verify','logout');
		//$this->Auth->allow();
    }
	
	
	public function login() {
        $this->layout = false;
        $this->autoRender = false;
		if($this->request->is('post') || $this->request->is('put')) {
		  $email =$this->request->data['email'];
		  $password =$this->request->data['password'];
		  $this->request->data['User']['email'] = $this->request->data['email'];
		  $this->request->data['User']['password'] = $this->request->data['password'];
		  $userData = $this->User->find('first',array('conditions'=>array('User.email'=>$email)));
			if($userData < 1){
				 $msg='User not exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
			    /*echo "<pre>";
                print_r($userData);
                die;*/
                if($userData['User']['verified'] == 0){
                  $msg='You are not verify your email address. Please Verify';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}
                if($userData['User']['status'] == 0){
                  $msg='You account has been deactivated. Please contact Administration.';
				  $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				  return json_encode($arr);
				}				
				if ($this->Auth->login()) {
					//$user = $this->Auth->identify();
					$user = $this->Session->read('Auth.User');
					$user_id  = $this->Session->read('Auth.User.id');
					//$sid = AuthComponent::password($user_id);
					$sid = trim(base64_encode($user_id));
					//$data = array("user"=> $user, "sid"=> $sid);
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Login Successfully',"data"=>$user,"sid"=>$sid);
                    return json_encode($arr);
				} else {
					 $msg='Invalid username or password.';
					 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					 return json_encode($arr);
				}
			}
		}
    }
	
	public function signup() {
        $this->layout = false;
        $this->autoRender = false;
		//echo 'Sign Up';	
        if ($this->request->is('post') || $this->request->is('put')) {
			$this->request->data['User']['name']=$this->request->data['name'];
			$this->request->data['User']['email']=$this->request->data['email'];
			$this->request->data['User']['password'] = AuthComponent::password($this->request->data['password']);
			$this->request->data['User']['status']=1;
			$name = $this->request->data['User']['name'];
			$email = $this->request->data['User']['email'];
			//echo "<pre>";
			//print_r($this->request->data);die;
			$email = $this->request->data['email'];
			$userData = $this->User->find('count',array('conditions'=>array('User.email'=>$email)));
			if($userData >= 1){
				 $msg='Email allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->User->save($this->request->data, false)) {
					$lastInsertId = $this->User->getLastInsertId();
					$activationKey = trim(base64_encode($lastInsertId));
					$email_template = '<table border="1" cellpadding="1" cellspacing="1" style="width:80%">
									<tbody>
										<tr>
											<td><strong>Hello [username],</strong></td>
										</tr>
										<tr>
											<td>
											<p>Welcome! You have successfully registered.\r\n</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Please verify your account by clicking bellow link.\r\n</p>
											<p>[link]</p>
											</td>
										</tr>
										<tr>
											<td>
											<p>Thanks!</p>

											<p>[site_title]</p>
											<p></p>
											</td>
										</tr>
									</tbody>
								</table>';
					//print_r($email_template);die;
					$url1 = Router::url(array('controller' => 'users', 'action' => 'account_verification',$activationKey), true);
                    $url = "<a href='$url1'>Click Here</a>";			
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
					$email_template = str_replace('[link]', $url, $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    //$email_template = str_replace('[password]', $pass, $email_template);
					$subject = 'Registration  Email';
					$sender_email = 'info@phishing.com';
					try{
                             $this->sendEmail($email, $sender_email, $subject, 'common', $email_template);
                             $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'User Created Successfully');
					         return json_encode($arr);
                      }catch(Exception $e){
                            $msg='Something went wrong! Please try again.';
							$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
							return json_encode($arr);
                        }
					
				}else{
					$msg='Something went wrong! Please try again.';
					$arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
					return json_encode($arr);
					}
			}	
        }
    }
	
	 public function forgot_password() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['User']['email'];
            if (!empty($email)) {
                $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'forgot_password')));

                $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
                $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
                $subject = $email_template_detail['EmailTemplate']['subject'];
                $sender_name = $email_template_detail['EmailTemplate']['sender_name'];

                $Userr = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if (!empty($Userr)) {
                    $newpassword = $this->__generatePassword();
                    //$Userr['User']['org_pass'] = $newpassword;
                    $Userr['User']['password'] = $this->Auth->password($newpassword);
                    $Userr['User']['id'];


                    $name = $Userr['User']['full_name'];
                    $email = $Userr['User']['email'];
                    $pass = $newpassword;
					
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    $email_template = str_replace('[password]', $pass, $email_template);
					
					try{
                            $this->sendEmail($this->request->data['User']['email'], $sender_email, $subject, 'common', $email_template);
                             if ($this->User->Save($Userr['User'])) {
								$this->Session->setFlash(__('Please check your email for reset password.'),'success');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
							}else{
								$this->Session->setFlash(__('unable to send email'),'error');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));	
							}
                        }catch(Exception $e){
                            //pr($e);
                        }
						
                } else {
                    $this->Session->setFlash(__('Invalid email.'));
                }
            } else {
                $this->Session->setFlash(__('Invalid email.'));
            }
        }
    }
	
	public function client_list() {
        $this->layout = false;
        $this->autoRender = false;
		if ($this->request->is('post') || $this->request->is('put')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			$result=array();
			$client = $this->Client->find('all',array('conditions'=>array('Client.user_id'=>$user_id),'order'=>'Client.id desc'));
			$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client List',"data"=>$client); 
		    echo json_encode($arr); die; 
        }		
    }
	
	public function client_add() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			$sid=$this->request->data['sid'];
			$user_id = base64_decode($sid);
			$client_name = $this->request->data['client_name'];
			$industry = $this->request->data['industry'];
			$official_website = $this->request->data['official_website'];
			$domain_name = $this->request->data['domain_name'];
			$this->request->data['Client']['user_id']=$user_id;
			$this->request->data['Client']['client_name']=$client_name;
			$this->request->data['Client']['industry'] = $industry;
			$this->request->data['Client']['official_website']= $official_website;
			$this->request->data['Client']['domain_name']= $domain_name;
			$clientData = $this->Client->find('count',array('conditions'=>array('Client.client_name'=>$client_name)));
			if($clientData >= 1){
				 $msg='This Client allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->Client->save($this->request->data, false)) {
					$clientid = $this->Client->getLastInsertID();
					if(exec("/usr/bin/python /var/www/html/python/dnstwist.py $domain_name $clientid")){
						$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Client Added and domain scanned Successfull. ');
					}else{
						$arr = array("statusCode" => "400", "replyStatus" => "success", "replyMessage" =>'Client Added Successfully, but python not executed');
					}
					
					return json_encode($arr);
				}	
			}	
        }
    }
	
	public function recently_added_domain() {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $domains = $this->DomainInfo->find('all',array('limit' => 12,'order'=>'DomainInfo.id desc'));
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Info List',"data"=>$domains); 
		echo json_encode($arr); die;  		
	}
	
	public function scan_result() {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $dnstwist = $this->Dnstwist->find('all');
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist List',"data"=>$dnstwist); 
		echo json_encode($arr); die;  		
    }
	
	public function add_domain_info() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$domain_name = $this->request->data['domain_name'];
			$query_time = $this->request->data['query_time'];
			$whois_server = $this->request->data['whois_server'];
			$domain_registered = $this->request->data['domain_registered'];
			$create_date = $this->request->data['create_date'];
			$update_date = $this->request->data['update_date'];
			$expiry_date = $this->request->data['expiry_date'];
			$status = $this->request->data['status'];
			
			$name_servers ='';
			$domain_status = '';
			$api_credits_charged = '';
			$whois_retrieval_time = '';
			$user_ip_address = '';
			if(!empty($this->request->data['name_servers'])){
				$name_servers = implode(",",$this->request->data['name_servers']);
			}
            if(!empty($this->request->data['domain_status'])){
				$domain_status = implode(",",$this->request->data['domain_status']);
			}
			if(!empty($this->request->data['api_credits_charged'])){
               $api_credits_charged = $this->request->data['api_credits_charged'];
			}   
			if(!empty($this->request->data['whois_retrieval_time'])){
               $whois_retrieval_time = $this->request->data['whois_retrieval_time'];
			}   
			if(!empty($this->request->data['user_ip_address'])){
               $user_ip_address = $this->request->data['user_ip_address'];
			}   
			$this->request->data['DomainInfo']['domain_name']=$domain_name;
			$this->request->data['DomainInfo']['query_time']=$query_time;
			$this->request->data['DomainInfo']['whois_server'] = $whois_server;
			$this->request->data['DomainInfo']['domain_registered']= $domain_registered;
			$this->request->data['DomainInfo']['create_date']= $create_date;
			$this->request->data['DomainInfo']['update_date']= $update_date;
			$this->request->data['DomainInfo']['expiry_date']= $expiry_date;
			$this->request->data['DomainInfo']['status']= $status;
			$this->request->data['DomainInfo']['name_servers']= $name_servers;
			$this->request->data['DomainInfo']['domain_status']= $domain_status;
			$this->request->data['DomainInfo']['api_credits_charged']= $api_credits_charged;
			$this->request->data['DomainInfo']['whois_retrieval_time']= $whois_retrieval_time;
			$this->request->data['DomainInfo']['user_ip_address']= $user_ip_address;
			$domainData = $this->DomainInfo->find('count',array('conditions'=>array('DomainInfo.domain_name'=>$domain_name)));
			if($domainData >= 1){
				 $msg='This domain name allready exist.';
				 $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
				 return json_encode($arr);
			}else{ 
				if ($this->DomainInfo->save($this->request->data, false)) {
					$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Added Successfully');
					return json_encode($arr);
				}	
			}	
        }
    }
	
	public function add_domain_registrar() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$domain_id = $this->request->data['domain_id'];
			$iana_id = $this->request->data['iana_id'];
			$registrar_name = $this->request->data['registrar_name'];
			$whois_server = $this->request->data['whois_server'];
			$website_url = $this->request->data['website_url'];
			$email_address = $this->request->data['email_address'];
			$phone_number = $this->request->data['phone_number'];
			$this->request->data['DomainRegistrar']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrar']['iana_id']=$iana_id;
			$this->request->data['DomainRegistrar']['registrar_name'] = $registrar_name;
			$this->request->data['DomainRegistrar']['whois_server']= $whois_server;
			$this->request->data['DomainRegistrar']['website_url']= $website_url;
			$this->request->data['DomainRegistrar']['email_address']= $email_address;
			$this->request->data['DomainRegistrar']['phone_number']= $phone_number;
			if ($this->DomainInfo->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrar Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_domain_registrant_contact() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$domain_id = $this->request->data['domain_id'];
			$full_name = $this->request->data['full_name'];
			$company_name = $this->request->data['company_name'];
			$mailing_address = $this->request->data['mailing_address'];
			$city_name = $this->request->data['city_name'];
			$state_name = $this->request->data['state_name'];
			$zip_code = $this->request->data['zip_code'];
			$country_name = $this->request->data['country_name'];
			$country_code = $this->request->data['country_code'];
			$email_address = $this->request->data['email_address'];
			$phone_number = $this->request->data['phone_number'];
			$fax_number = $this->request->data['fax_number'];
			$this->request->data['DomainRegistrantContact']['domain_id']=$domain_id;
			$this->request->data['DomainRegistrantContact']['full_name']=$full_name;
			$this->request->data['DomainRegistrantContact']['company_name'] = $company_name;
			$this->request->data['DomainRegistrantContact']['mailing_address']= $mailing_address;
			$this->request->data['DomainRegistrantContact']['city_name']= $city_name;
			$this->request->data['DomainRegistrantContact']['state_name']= $state_name;
			$this->request->data['DomainRegistrantContact']['zip_code']= $zip_code;
			$this->request->data['DomainRegistrantContact']['country_name']= $country_name;
			$this->request->data['DomainRegistrantContact']['country_code']= $country_code;
			$this->request->data['DomainRegistrantContact']['email_address']= $email_address;
			$this->request->data['DomainRegistrantContact']['phone_number']= $phone_number;
			$this->request->data['DomainRegistrantContact']['fax_number']= $fax_number;
			if ($this->DomainRegistrantContact->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Domain Registrant Contact Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	public function add_virus_total_info() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$client_id =1;
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$scan_id = $this->request->data['scan_id'];
			$resource = $this->request->data['resource'];
			$url  = $this->request->data['url'];
			$scan_date = $this->request->data['scan_date'];
			$permalink  = $this->request->data['permalink'];
			$positives = $this->request->data['positives'];
			$total = $this->request->data['total'];
			$scans = $this->request->data['scans'];
			$this->request->data['VirusTotal']['client_id']=$client_id;
			$this->request->data['VirusTotal']['scan_id']=$scan_id;
			$this->request->data['VirusTotal']['resource'] = $resource;
			$this->request->data['VirusTotal']['url']= $url;
			$this->request->data['VirusTotal']['scan_date']= $scan_date;
			$this->request->data['VirusTotal']['permalink']= $permalink;
			$this->request->data['VirusTotal']['positives']= $positives;
			$this->request->data['VirusTotal']['total']= $total;
			// $this->request->data['VirusTotal']['scans']= $scans;
			if ($this->VirusTotal->save($this->request->data, false)) {
				$lastInsertId = $this->VirusTotal->getLastInsertId();
				$this->add_virus_total_scan($this->request->data['scans'],$lastInsertId);
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Virus total scan Added Successfully');
				return json_encode($arr);
			}		
        }
	}
	
	public function add_virus_total_scan($data1,$id){
		$this->layout = false;
        $this->autoRender = false;
		$this->request->data['VirusTotalScan']['virus_total_scan_id'] = $id;
        $keys = array_keys($data1);
        $count=0;
        foreach($data1 as $key){
			$this->VirusTotalScan->create(false);
			$this->request->data['VirusTotalScan']['site'] = $keys[$count];
			$this->request->data['VirusTotalScan']['detected'] = $key['detected']?'TRUE':'FALSE';
			$this->request->data['VirusTotalScan']['result'] = $key['result'];
			$this->VirusTotalScan->saveAll($this->request->data['VirusTotalScan']);
            $count++;
        } 
	}
	
	public function add_dns_twist() {
        $this->layout = false;
        $this->autoRender = false;	
        if ($this->request->is('post') || $this->request->is('put')) {
			//$sid=$this->request->data['sid'];
			//$user_id = base64_decode($sid);
			$client_id ='';
			if(!empty($this->request->data['client_id'])){
				$client_id = $this->request->data['client_id'];
			}	
			$fuzzer  = $this->request->data['fuzzer'];
			$domain_name = $this->request->data['domain-name'];
			$dns_a  = $this->request->data['dns-a']?implode(",",$this->request->data['dns-a']):'';
			$this->request->data['Dnstwist']['client_id']=$client_id;
			$this->request->data['Dnstwist']['fuzzer']=$fuzzer;
			$this->request->data['Dnstwist']['domain_name'] = $domain_name;
			$this->request->data['Dnstwist']['dns_a']= $dns_a;
			if ($this->Dnstwist->save($this->request->data, false)) {
				$arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'Dnstwist Added Successfully');
				return json_encode($arr);
			}		
        }
    }
	
	public function logout() {
        $this->Session->setFlash('Logout Successfully.','success');
        $this->Auth->logout();
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

 

    public function admin_search() {
        $leftnav = "users";
        $subleftnav = "view_user";
        $this->set(compact('leftnav', 'subleftnav'));

        $pageTitle = "Settings";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));

        if (!isset($this->params->query['status'])) {
            $this->params->query['status'] = '';
        }

        $condition = array('User.role_id' => 2);
        if (!empty($this->params->query['status']) && $this->params->query['status'] == '1') {
            $condition['User.status'] = $this->params->query['status'];
        } elseif ($this->params->query['status'] == '0') {
            $condition['User.status'] = $this->params->query['status'];
        } elseif ($this->params->query['status'] == 'FEATURED') {
            $condition['User.featured'] = 1;
        }


        if (!empty($this->params->query['name'])) {
            $name = Sanitize::clean($this->params->query['name'], array('encode' => false));
            $condition['OR']['User.email like '] = '%' . $name . '%';
            $condition['OR']['User.username like '] = '%' . $name . '%';
            $condition['OR']['User.name like '] = '%' . $name . '%';
        }

        $this->paginate = array('conditions' => $condition, 'limit' => 10);
        $user = $this->paginate('User');
        $this->set('users', $user);
    }

    public function admin_add() {
        if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $leftnav = "user-add";
        $subleftnav = "add";
         $pageTitle = "Add User";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));
        $this->set('pageHeading', 'Add Reporter');
        $countrylist = $this->Country->find('list',array('fields'=>array('Country.id','Country.countryName')));
		$this->set('countrylist', $countrylist);
        if ($this->request->is('post') || $this->request->is('put')) {
            try {
				$this->request->data['User']['role_id']=2;
				$this->request->data['User']['status']=1;
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['pass']);
                if ($this->User->save($this->request->data)) {
                    $this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('USER_CREATED'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                } else {
                    $msg = "";
                    foreach ($this->User->validationErrors as $value) {
                        $msg .=$value[0] . "<br/>";
                    }
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__($msg));
                }
            } catch (Exception $e) {
                $this->log($e, "debug");
            }
        }
    }
	
	public function admin_sync_data() {
		$this->layout = false;
        $this->autoRender = false;
		$arr=array();
		$userList=array();
        if ($this->request->is('post') || $this->request->is('put')) {
			$userList=$this->request->data;
			if(empty($userList)){
		      $msg='Please enter json data.';
		      $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
		      return json_encode($arr);
		    }
			if(!empty($userList)){
			   foreach($userList as $user){
                $usercount=$this->User->find('count',array('conditions'=>array('User.username'=>$user['username'],'User.device_id'=>$user['device_id'])));
				 if($usercount==0){
					$this->request->data['User']['role_id']=2;
					$this->request->data['User']['status']=1;
					$this->request->data['User']['full_name']=$user['full_name'];   
					$this->request->data['User']['username']=$user['username'];   
					$this->request->data['User']['dob']=$user['dob'];   
					$this->request->data['User']['device_id']=$user['device_id'];   
					$this->request->data['User']['device_token']=$user['device_token'];   
					$this->request->data['User']['timestamp']=$user['timestamp'];
					$this->User->saveAll($this->request->data['User']);	
				} 
			   }		   
			}
        $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" =>'user list sync to server.');
        return json_encode($arr);   			
        }else{
			$msg='Please enter json user data.';
		    $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
		    return json_encode($arr);
		}
    }
	
	public function admin_get_user_list($device_id=null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $users = $this->User->find('all',array('conditions'=>array('User.device_id'=>$device_id,'User.role_id !='=>1),'order'=>'User.id desc'));
		if(!empty($users)){
		 foreach($users as $key=>$user){
			$result[$key]['full_name'] =$user['User']['full_name'];
			$result[$key]['username'] = $user['User']['username'];
			$result[$key]['dob'] = $user['User']['dob'];
			$result[$key]['device_id'] = $user['User']['device_id'];
			$result[$key]['device_token'] = $user['User']['device_token'];
			$result[$key]['timestamp'] = $user['User']['timestamp'];
		 }		
		}
       return json_encode($result);		
    }
	

    public function admin_edit($id = null) {
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $this->layout = 'admin';
        $leftnav = "users";
        $subleftnav = "view_user";
      	$pageTitle = "Edit User";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));
        $this->set('pageHeading', 'Edit user');
        $countrylist = $this->Country->find('list',array('fields'=>array('Country.id','Country.countryName')));
		$this->set('countrylist', $countrylist);
        if (empty($id) && empty($this->request->data)) {
            $this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Invalid User'));
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }

        try {
            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->User->save($this->request->data)) {
                    $this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('User Updated'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                } else {
                    $msg = "";
                    foreach ($this->User->validationErrors as $value) {
                        $msg .=$value[0] . "<br/>";
                    }
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__($msg));
                }
            } else {
                $this->User->id = $id;
                if (!$this->User->exists()) {
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Invalid User'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                }
                $this->request->data = $this->User->read(null, $id);
                unset($this->request->data['User']['password']);
            }
        } catch (Exception $e) {
            $this->log($e, "debug");
        }
       
    }


    public function admin_delete($id = null) {
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $this->layout = false;
        try {
            $this->User->delete($id);
            $this->Session->write('msg_type', 'alert-success');
            $this->Session->setFlash(__('User Deleted successfully'));
        } catch (Exception $e) {
            $this->log($e, 'debug');
            $this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Error while deleting user'));
        }
        $this->redirect(array('controller' => 'users', 'action' => 'index'));
    }
	
    public function admin_forgot() {
        $this->layout = 'admin_login';
        $leftnav = "users";
        $pageTitle = "Forgot Password";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['User']['email'];
            if (!empty($email)) {
                $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'forgot_password')));

                $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
                $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
                $subject = $email_template_detail['EmailTemplate']['subject'];
                $sender_name = $email_template_detail['EmailTemplate']['sender_name'];

                $Userr = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if (!empty($Userr)) {
                    $newpassword = $this->__generatePassword();
                    //$Userr['User']['org_pass'] = $newpassword;
                    $Userr['User']['password'] = $this->Auth->password($newpassword);
                    $Userr['User']['id'];


                    $name = $Userr['User']['full_name'];
                    $email = $Userr['User']['email'];
                    $pass = $newpassword;
					
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    $email_template = str_replace('[password]', $pass, $email_template);
					
					try{
                            $this->sendEmail($this->request->data['User']['email'], $sender_email, $subject, 'common', $email_template);
                             if ($this->User->Save($Userr['User'])) {
								$this->Session->setFlash(__('Please check your email for reset password.'),'success');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
							}else{
								$this->Session->setFlash(__('unable to send email'),'error');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));	
							}
                        }catch(Exception $e){
                            //pr($e);
                        }
						
                } else {
                    $this->Session->setFlash(__('Invalid email.'));
                }
            } else {
                $this->Session->setFlash(__('Invalid email.'));
            }
        }
    }
	

 public function admin_setting() {

        $leftnav = "setting";
        $subleftnav = "setting";
		$title_for_layout = $pageTitle = "Change Password";
		$this->set(compact('leftnav', 'subleftnav','pageTitle','title_for_layout'));

        $this->loadModel('Setting');
        $id = $this->Auth->user('id');

        if (!$id && empty($this->request->data)) {
			$this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Invalid User'));
            $this->redirect(array('action' => 'setting'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            //validate data
            $this->User->set($this->request->data);
            if ($this->User->validates()) {
                
                if ($this->request->data['User']['password'] != $this->request->data['User']['re_password']) {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Please enter confirm password'));
                    $this->redirect(array('action' => 'setting'));
                }
                $this->request->data['User']['id']= $id;
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

                if ($this->User->save($this->request->data)) {
					$this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('Password has been reset.'));
                    $this->redirect(array('action' => 'setting'));
                } else {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Password could not be reset. Please, try again.'));
                }
            }
        }

        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);
			$this->request->data['User']['password'] = '';
			$this->request->data['User']['old_image']= $this->request->data['User']['image'];
        }
		//$this->set('settings', $this->Setting->find('all', array('conditions' => array('status' => 1))));
    }

   

	public function verify($userid){
	    $this->layout = false;
        $this->autoRender = false;	
        $userId = base64_decode($userid);
        $userData['User']['id'] = $userId;
        $userData['User']['verified'] = 1;
        $checkuser = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
        if($checkuser){
            if ($this->User->save($userData['User'], false)) {
                $sendarray = array("statusCode"=>200,"replyMessage" => "User verified successfully.","usre_id"=>$userid);
            }else{
                $sendarray = array("statusCode"=>201,"replyMessage" => "User not verified successfully.");
            }
        }else{
            $sendarray = array("statusCode"=>201,"replyMessage" => "User not exist.");
        }
         
        echo json_encode($sendarray);die;
    }

	
	public function admin_profile(){
		$leftnav = "";
        $subleftnav = "";
        $title_for_layout = $pageTitle = 'Profile';
        $pageHeading = 'Profile';
		$session_id = $this->Session->read('Auth.User.id');
		$data = $this->User->find('first',array('conditions'=>array('User.id'=>$session_id)));
		
		if(!empty($this->request->data)){
			$this->User->id = $session_id;
			if($this->User->save($this->request->data, false)){
				$this->Session->write('msg_type', 'alert-success');
				$this->Session->setFlash('Profile updated successfully');
			}
		}else{
			$this->User->id = $session_id;
			$this->request->data = $this->User->read();
		}
		
		$this->set(compact('leftnav', 'subleftnav','pageTitle','pageHeading','data','title_for_layout'));
	}
	
	public function admin_upload_image(){
		if($this->request->is('ajax')){
			$session_id = $this->Session->read('Auth.User.id');
			$result['status'] = 0;
			$result['image'] = '';
			if(!empty($this->request->data['image'])){
				$destination = realpath('../../app/webroot/uploads/users/');
				$file_name = $this->base64toimage($this->request->data['image'], $destination);
				if(!empty($file_name)){
					$this->User->id = $session_id;
					$this->User->save(array('image'=>$file_name), false);
					$result['status'] = 1;
					$result['image'] = WEBSITE_URL.'uploads/users/'.$file_name;
					if($this->Session->check('Auth.User.image') && $this->Session->read('Auth.User.image') != '' && file_exists($destination.'/'.$this->Session->read('Auth.User.image'))){
						unlink($destination.'/'.$this->Session->read('Auth.User.image'));
					}
					$this->Session->write('Auth.User.image',$file_name);
				}
			}
			echo json_encode($result); die;
		}
	}
	
	public function base64toimage($imagecode,$destination) {
       // Image upload /
        if (strpos($imagecode, "png;base64,") != 0)
            $filename = md5(rand()) . ".png";
        else
            $filename = md5(rand()) . ".jpeg";

		$dirpath = $destination;
        $mainImgDirPath = $dirpath . DS . $filename;
        $data_new = $imagecode;
        $pos = strpos($data_new, "base64,");
        if (isset($pos) && $pos != "") {
            $img = substr($data_new, $pos + 7, strlen($data_new));
        } else {
            $img = $data_new;
        }
        $img = str_replace(' ', '+', $img);
        $svdata = base64_decode($img);

        $fp = fopen($mainImgDirPath, 'w');
        if (fwrite($fp, $svdata)) {
            fclose($fp);
        } else {
            $mainImgDirPath = '';
        }
        return $filename;
        // Image upload End /
    }
	
	public function verification($code = null){
		if(!empty($code)){
			$user_data = $this->User->find('first',array('conditions'=>array('User.verification_code'=>$code),'fields'=>array('User.id')));
			if(!empty($user_data)){
				$data['User']['id'] = $user_data['User']['id'];
				$data['User']['status'] = 1;
				$data['User']['verification_code'] = '';
				
                if($this->User->save($data)){
					$this->Session->setFlash(__('You are successfully verified you account, Please login'));
					$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
				}
			}else{
					$this->Session->setFlash(__('Invalid token'));
					$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
			}
		}
    }
    
    public function admin_deleteuser() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $username= "";
            $device_id= "";
            if(isset($this->request->data['username']) && !empty($this->request->data['username'])){
                $username =$this->request->data['username'];
            }
            if(isset($this->request->data['device_id']) && !empty($this->request->data['device_id'])){
                $device_id =$this->request->data['device_id'];
            }     
            if(!empty($username) && !empty($device_id)){
                $this->User->deleteAll(array('User.username'=>$username,'User.device_id'=>$device_id),false);
            }
            if(!empty($username) && empty($device_id)){
                $this->User->deleteAll(array('User.username'=>$username),false);
            }
            if(empty($username) && !empty($device_id)){
                $this->User->deleteAll(array('User.device_id'=>$device_id),false);
            }
            $msg='User Data deleted succesfully.';
            $arr = array("statusCode" => "200", "replyStatus" => "success", "replyMessage" => __($msg));	
		    return json_encode($arr);
        }else{
            $msg='Please enter user data.';
            $arr = array("statusCode" => "400", "replyStatus" => "error", "replyMessage" => __($msg));	
            return json_encode($arr);
        }    
    }
	
  public function getToken($length){
        //Generate a random string.
        $token = openssl_random_pseudo_bytes(50);
        //Convert the binary data into hexadecimal representation.
        $token = bin2hex($token);
        return $token;
   }
	
	
}
