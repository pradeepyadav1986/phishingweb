<?php

App::uses('Sanitize', 'Utility');

class UsersController extends AppController {

    public $name = 'Users';
    public $uses = array('User', 'EmailTemplate','Category','Image','Country');
    public $helpers = array('Html', 'Form', 'Session','Common');
    public $components = array('Cookie', 'Email','Json','Upload');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('admin_login', 'admin_forgot', 'verify_email','login','admin_sync_data','admin_get_user_list','admin_deleteuser');
    }
	
	
	public function login() {
        $title_for_layout =  'Login';
		
        $this->layout = 'login';
        if ($this->Session->read('Auth.User')) {
            $this->redirect(array('controller' => 'zones', 'action' => 'index'));
        }
		
        if ($this->request->is('post')) {
            
			$userData = $this->User->find('count',array('conditions'=>array('User.email'=>$this->request->data['User']['email'])));
			if($userData < 1){
				$this->Session->setFlash(__('You are not authorusized'),'error');
			}else{ 
				if ($this->Auth->login()) {
					if (isset($this->request->data['User']['remmber_me']) && $this->request->data['User']['remmber_me'] == 1) {
						$this->Cookie->delete('Auth');
						$cookie = array();
						$cookie['username'] = $this->request->data['User']['email'];
						$cookie['password'] = $this->request->data['User']['password'];
						$this->Cookie->write('Auth.User', $cookie, true, '+2 weeks');
						unset($this->request->data['User']['remember_me']);
					} else {
						$this->Cookie->delete('Auth');
					}
					$this->redirect(array('controller' => 'zones', 'action' => 'index'));
				} else {
					$this->Session->setFlash(__('Invalid email or password.'),'error');
				}
			}
        }
        $cookie = $this->Cookie->read('Auth');
        if (!empty($cookie)) {
            $this->request->data['User']['email'] = $cookie['User']['username'];
            $this->request->data['User']['password'] = $cookie['User']['password'];
            $this->request->data['User']['remmber_me'] = 1;
        }

        $this->set(compact('title_for_layout'));
    }
	
	public function logout() {
        $this->Session->setFlash('Logout Successfully.','success');
        $this->Auth->logout();
        $this->redirect(array('controller' => 'users', 'action' => 'login'));
    }

    public function admin_login() {
        $title_for_layout =  'Login';
		
        $this->layout = 'admin_login';
        if ($this->Session->read('Auth.User')) {
            $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
        }
		
        if ($this->request->is('post')) {
			$userData = $this->User->find('count',array('conditions'=>array('User.email'=>$this->request->data['User']['email'], 'OR'=>array('User.role_id'=>array(1,2)))));
			if($userData < 1){
				$this->Session->setFlash(__('You are not authorusized'),'error');
			}else{ 
				if ($this->Auth->login()) {
					if (isset($this->request->data['User']['remmber_me']) && $this->request->data['User']['remmber_me'] == 1) {
						$this->Cookie->delete('Auth');
						$cookie = array();
						$cookie['username'] = $this->request->data['User']['email'];
						$cookie['password'] = $this->request->data['User']['password'];
						$this->Cookie->write('Auth.User', $cookie, true, '+2 weeks');
						unset($this->request->data['User']['remember_me']);
					} else {
						$this->Cookie->delete('Auth');
					}
					$this->redirect(array('controller' => 'users', 'action' => 'dashboard'));
				} else {
					$this->Session->setFlash(__('Invalid username or password.'),'error');
				}
			}
        }
        $cookie = $this->Cookie->read('Auth');
        if (!empty($cookie)) {
            $this->request->data['User']['email'] = $cookie['User']['username'];
            $this->request->data['User']['password'] = $cookie['User']['password'];
            $this->request->data['User']['remmber_me'] = 1;
        }

        $this->set(compact('title_for_layout'));
    }

    public function admin_logout() {
        $this->Session->setFlash('Logout Successfully.','success');
        $this->Auth->logout();
        $this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
    }

    public function admin_dashboard() {
		$leftnav = "dashboard";
        $title_for_layout = $pageTitle = "Dashboard";
		//$TotalSubadmin = $this->User->find('count',array('conditions'=>array('User.role_id'=>2)));
		$TotalUsers = $this->User->find('count',array('conditions'=>array('User.role_id'=>2)));
		//$TotalImages = $this->Image->find('count');
		$this->set(compact('leftnav','pageTitle','title_for_layout','TotalUsers'));
    }

    public function admin_index() {
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $leftnav = "users";
        $subleftnav = "view_user";
		$pageTitle = "Users";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));

        $this->set('pageHeading', $pageTitle);

        $this->paginate = array('conditions' => array('User.role_id' => 2), 'limit' => 10,'order'=>'User.id desc');
        $user = $this->paginate('User');
        $this->set('users', $user);
		//pr(json_encode($user));die;
    }

	function admin_view($id = null){
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
		$leftnav = "users";
        $subleftnav = "";
        $pageTitle = $pageHeading =  'View Users';
        $this->set(compact('leftnav', 'subleftnav','pageTitle','pageHeading'));
		$this->User->id = $id;
		if (!$this->User->exists()) {
			$this->redirect(array('controller' => 'Users', 'action' => 'index'));
		}
		$UserData = $this->User->read(null, $id);
		$this->set(compact('UserData'));
	}


    public function admin_search() {
        $leftnav = "users";
        $subleftnav = "view_user";
        $this->set(compact('leftnav', 'subleftnav'));

        $pageTitle = "Settings";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));

        if (!isset($this->params->query['status'])) {
            $this->params->query['status'] = '';
        }

        $condition = array('User.role_id' => 2);
        if (!empty($this->params->query['status']) && $this->params->query['status'] == '1') {
            $condition['User.status'] = $this->params->query['status'];
        } elseif ($this->params->query['status'] == '0') {
            $condition['User.status'] = $this->params->query['status'];
        } elseif ($this->params->query['status'] == 'FEATURED') {
            $condition['User.featured'] = 1;
        }


        if (!empty($this->params->query['name'])) {
            $name = Sanitize::clean($this->params->query['name'], array('encode' => false));
            $condition['OR']['User.email like '] = '%' . $name . '%';
            $condition['OR']['User.username like '] = '%' . $name . '%';
            $condition['OR']['User.name like '] = '%' . $name . '%';
        }

        $this->paginate = array('conditions' => $condition, 'limit' => 10);
        $user = $this->paginate('User');
        $this->set('users', $user);
    }

    public function admin_add() {
        if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $leftnav = "user-add";
        $subleftnav = "add";
         $pageTitle = "Add User";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));
        $this->set('pageHeading', 'Add Reporter');
        $countrylist = $this->Country->find('list',array('fields'=>array('Country.id','Country.countryName')));
		$this->set('countrylist', $countrylist);
        if ($this->request->is('post') || $this->request->is('put')) {
            try {
				$this->request->data['User']['role_id']=2;
				$this->request->data['User']['status']=1;
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['pass']);
                if ($this->User->save($this->request->data)) {
                    $this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('USER_CREATED'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                } else {
                    $msg = "";
                    foreach ($this->User->validationErrors as $value) {
                        $msg .=$value[0] . "<br/>";
                    }
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__($msg));
                }
            } catch (Exception $e) {
                $this->log($e, "debug");
            }
        }
    }
	
	public function admin_sync_data() {
		$this->layout = false;
        $this->autoRender = false;
		$arr=array();
		$userList=array();
        if ($this->request->is('post') || $this->request->is('put')) {
			$userList=$this->request->data;
			if(empty($userList)){
		      $msg='Please enter json data.';
		      $arr = array("replyCode" => "400", "replyStatus" => "error", "replyMsg" => __($msg));	
		      return json_encode($arr);
		    }
			if(!empty($userList)){
			   foreach($userList as $user){
                $usercount=$this->User->find('count',array('conditions'=>array('User.username'=>$user['username'],'User.device_id'=>$user['device_id'])));
				 if($usercount==0){
					$this->request->data['User']['role_id']=2;
					$this->request->data['User']['status']=1;
					$this->request->data['User']['full_name']=$user['full_name'];   
					$this->request->data['User']['username']=$user['username'];   
					$this->request->data['User']['dob']=$user['dob'];   
					$this->request->data['User']['device_id']=$user['device_id'];   
					$this->request->data['User']['device_token']=$user['device_token'];   
					$this->request->data['User']['timestamp']=$user['timestamp'];
					$this->User->saveAll($this->request->data['User']);	
				} 
			   }		   
			}
        $arr = array("replyCode" => "200", "replyStatus" => "success", "replyMsg" =>'user list sync to server.');
        return json_encode($arr);   			
        }else{
			$msg='Please enter json user data.';
		    $arr = array("replyCode" => "400", "replyStatus" => "error", "replyMsg" => __($msg));	
		    return json_encode($arr);
		}
    }
	
	public function admin_get_user_list($device_id=null) {
        $this->layout = false;
        $this->autoRender = false;
		$result=array();
        $users = $this->User->find('all',array('conditions'=>array('User.device_id'=>$device_id,'User.role_id !='=>1),'order'=>'User.id desc'));
		if(!empty($users)){
		 foreach($users as $key=>$user){
			$result[$key]['full_name'] =$user['User']['full_name'];
			$result[$key]['username'] = $user['User']['username'];
			$result[$key]['dob'] = $user['User']['dob'];
			$result[$key]['device_id'] = $user['User']['device_id'];
			$result[$key]['device_token'] = $user['User']['device_token'];
			$result[$key]['timestamp'] = $user['User']['timestamp'];
		 }		
		}
       return json_encode($result);		
    }
	

    public function admin_edit($id = null) {
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $this->layout = 'admin';
        $leftnav = "users";
        $subleftnav = "view_user";
      	$pageTitle = "Edit User";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));
        $this->set('pageHeading', 'Edit user');
        $countrylist = $this->Country->find('list',array('fields'=>array('Country.id','Country.countryName')));
		$this->set('countrylist', $countrylist);
        if (empty($id) && empty($this->request->data)) {
            $this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Invalid User'));
            $this->redirect(array('controller' => 'users', 'action' => 'index'));
        }

        try {
            if ($this->request->is('post') || $this->request->is('put')) {
                if ($this->User->save($this->request->data)) {
                    $this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('User Updated'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                } else {
                    $msg = "";
                    foreach ($this->User->validationErrors as $value) {
                        $msg .=$value[0] . "<br/>";
                    }
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__($msg));
                }
            } else {
                $this->User->id = $id;
                if (!$this->User->exists()) {
                    $this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Invalid User'));
                    $this->redirect(array('controller' => 'users', 'action' => 'index'));
                }
                $this->request->data = $this->User->read(null, $id);
                unset($this->request->data['User']['password']);
            }
        } catch (Exception $e) {
            $this->log($e, "debug");
        }
       
    }


    public function admin_delete($id = null) {
		if($this->Session->read('Auth.User.role_id') != 1){
		   $this->Session->setFlash(__('You are not authorusized to access this location'),'error');
		   $this->redirect(array('controller' => 'users', 'action' => 'dashboard'));	
		}
        $this->layout = false;
        try {
            $this->User->delete($id);
            $this->Session->write('msg_type', 'alert-success');
            $this->Session->setFlash(__('User Deleted successfully'));
        } catch (Exception $e) {
            $this->log($e, 'debug');
            $this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Error while deleting user'));
        }
        $this->redirect(array('controller' => 'users', 'action' => 'index'));
    }
	
    public function admin_forgot() {
        $this->layout = 'admin_login';
        $leftnav = "users";
        $pageTitle = "Forgot Password";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $email = $this->request->data['User']['email'];
            if (!empty($email)) {
                $email_template_detail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.email_type ' => 'forgot_password')));

                $email_template = nl2br($email_template_detail['EmailTemplate']['message']);
                $sender_email = $email_template_detail['EmailTemplate']['sender_email'];
                $subject = $email_template_detail['EmailTemplate']['subject'];
                $sender_name = $email_template_detail['EmailTemplate']['sender_name'];

                $Userr = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if (!empty($Userr)) {
                    $newpassword = $this->__generatePassword();
                    //$Userr['User']['org_pass'] = $newpassword;
                    $Userr['User']['password'] = $this->Auth->password($newpassword);
                    $Userr['User']['id'];


                    $name = $Userr['User']['full_name'];
                    $email = $Userr['User']['email'];
                    $pass = $newpassword;
					
                    $email_template = str_replace('[site_title]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[sitename]', Configure::read('Site.title'), $email_template);
                    $email_template = str_replace('[username]', $name, $email_template);
                    $email_template = str_replace('[email]', $email, $email_template);
                    $email_template = str_replace('[password]', $pass, $email_template);
					
					try{
                            $this->sendEmail($this->request->data['User']['email'], $sender_email, $subject, 'common', $email_template);
                             if ($this->User->Save($Userr['User'])) {
								$this->Session->setFlash(__('Please check your email for reset password.'),'success');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
							}else{
								$this->Session->setFlash(__('unable to send email'),'error');
								$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));	
							}
                        }catch(Exception $e){
                            //pr($e);
                        }
						
                } else {
                    $this->Session->setFlash(__('Invalid email.'));
                }
            } else {
                $this->Session->setFlash(__('Invalid email.'));
            }
        }
    }
	

	public function admin_usersetting($id = null) {

        $leftnav = "User setting";
        $subleftnav = "User setting";
		$title_for_layout = $pageTitle = "User Change Password";
		$this->set(compact('leftnav', 'subleftnav','pageTitle','title_for_layout'));

        $this->loadModel('Setting');
        if (!$id && empty($this->request->data)) {
			$this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Invalid User'));
            $this->redirect(array('action' => 'usersetting'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            //validate data
            $this->User->set($this->request->data);
            if ($this->User->validates()) {
                
                if ($this->request->data['User']['password'] != $this->request->data['User']['re_password']) {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Please enter confirm password'));
                    $this->redirect(array('action' => 'usersetting'));
                }
                $this->request->data['User']['id']= $id;
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

                if ($this->User->save($this->request->data)) {
					$this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('Password has been reset.'));
                    $this->redirect(array('action' => 'index'));
                } else {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Password could not be reset. Please, try again.'));
                }
            }
        }

        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);
			$this->request->data['User']['password'] = '';
			$this->request->data['User']['old_image']= $this->request->data['User']['image'];
        }
		//$this->set('settings', $this->Setting->find('all', array('conditions' => array('status' => 1))));
    }

    public function admin_setting() {

        $leftnav = "setting";
        $subleftnav = "setting";
		$title_for_layout = $pageTitle = "Change Password";
		$this->set(compact('leftnav', 'subleftnav','pageTitle','title_for_layout'));

        $this->loadModel('Setting');
        $id = $this->Auth->user('id');

        if (!$id && empty($this->request->data)) {
			$this->Session->write('msg_type', 'alert-danger');
            $this->Session->setFlash(__('Invalid User'));
            $this->redirect(array('action' => 'setting'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            //validate data
            $this->User->set($this->request->data);
            if ($this->User->validates()) {
                
                if ($this->request->data['User']['password'] != $this->request->data['User']['re_password']) {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Please enter confirm password'));
                    $this->redirect(array('action' => 'setting'));
                }
                $this->request->data['User']['id']= $id;
                $this->request->data['User']['password'] = AuthComponent::password($this->request->data['User']['password']);

                if ($this->User->save($this->request->data)) {
					$this->Session->write('msg_type', 'alert-success');
                    $this->Session->setFlash(__('Password has been reset.'));
                    $this->redirect(array('action' => 'setting'));
                } else {
					$this->Session->write('msg_type', 'alert-danger');
                    $this->Session->setFlash(__('Password could not be reset. Please, try again.'));
                }
            }
        }

        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);
			$this->request->data['User']['password'] = '';
			$this->request->data['User']['old_image']= $this->request->data['User']['image'];
        }
		//$this->set('settings', $this->Setting->find('all', array('conditions' => array('status' => 1))));
    }

    public function admin_setting_update() {
        $leftnav = "sitesettings";
        $subleftnav = "index";
        $this->loadModel('Setting');
        $this->set(compact('leftnav', 'subleftnav'));
        if ($this->request->is(array('post', 'put'))) {

            if ($this->Setting->saveAll($this->request->data['Setting'], array('validate' => false))) {
                $this->Setting->writeConfiguration();
                $this->Session->setFlash('Site Settings saved successfully.', 'default', array('class' => 'success'));
                $this->redirect(array('controller' => 'users', 'action' => 'setting'));
            } else {
                $this->Session->setFlash('Site Settings could not be saved. Please try again.', 'default', array('class' => 'error'));
                $this->redirect(array('controller' => 'users', 'action' => 'setting'));
            }
        } else {
            $this->Session->setFlash('Site Settings could not be saved. Please try again.', 'default', array('class' => 'error'));
            $this->redirect(array('controller' => 'users', 'action' => 'setting'));
        }
    }



    /* general settings */
    public function admin_general_settings() {
        $leftnav = "setting";
        $subleftnav = "view_user";

		$pageTitle = "Settings";
	    $this->set(compact('leftnav', 'subleftnav','pageTitle'));
        $id = $this->Auth->user('id');
        $this->layout = 'admin';
        if (!$id && empty($this->request->data)) {
            $this->Session->setFlash(__('Invalid User'), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'setting'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            //validate data
            $this->User->set($this->request->data);
            if ($this->User->validates()) {

                $destination = realpath('../../app/webroot/uploads/user_pictures/') . '/';
                $file = $this->request->data['User']['image'];

                if ($file['name']!= "") {
                    $this->Upload->upload($file, $destination, null);
                    $errors = $this->Upload->errors;

                    if (empty($errors)) {
                        $this->request->data['User']['image'] = $this->Upload->result;
						$this->Session->write('Auth.User.image',$this->request->data['User']['image']);
                    } else {
                        if (is_array($errors)) {
                            $errors = implode("<br />", $errors);
                        }

                        $this->Session->write('msg_type', 'alert-danger');
                        $this->Session->setFlash($errors);
						return;
                    }

					$destination = realpath('../../app/webroot/uploads/user_pictures/') . '/';
					$userfile = $this->request->data['User']['old_image'];
					if($userfile!=""){
						if(file_exists($destination.$userfile)){
							unlink($destination.$userfile);
						}
					}
                }else{
                    $this->request->data['User']['image'] = $this->request->data['User']['old_image'];
                }

                if ($this->User->save($this->request->data)) {
                    $this->Session->write('Auth.User.full_name', $this->request->data['User']['full_name']);
                    $this->Session->setFlash(__('General settings saved succesfully'), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'setting'));
                }
            }
        }
        if (empty($this->request->data)) {
            $this->request->data = $this->User->read(null, $id);

        }
    }

	public function verify_email($code = ''){
		$this->layout=false;
		$leftnav = "users";
        $subleftnav = "add";
        $pageTitle = "Verify Email-Id";
        $this->set(compact('leftnav', 'subleftnav','pageTitle'));
		try {
			$users = $this->User->find('first', array('conditions' => array('User.verifiy_code' => $code, 'role_id' => 2), 'fields' => array('id')));
			if(count($users) > 0){
				//$userid = $users['User']['id'];
				$users['User']['verifiy_code'] = "";
				$users['User']['email_verified'] = 1;
				//pr($users);
				if ($this->User->save($users)) {
					$msg =  "You have succesfully verified your account.";
					//$this->Session->write('msg_type', 'alert-success');
					//$this->Session->setFlash(__('USER_UPDATED'));
					//$this->redirect(array('controller' => 'users', 'action' => 'index'));
				}else{
					$msg = "";
					foreach ($this->User->validationErrors as $value) {
						$msg .=$value[0] . "<br/>";
					}
					$msg =  "Something Error in your request.";
				}
			}else{
				$msg = "Your Code is not Corret Or already Expired.";
			}
		}catch (Exception $e) {
			$this->log($e, "debug");
		}
		$this->set('message',$msg);
	}

	
	public function admin_profile(){
		$leftnav = "";
        $subleftnav = "";
        $title_for_layout = $pageTitle = 'Profile';
        $pageHeading = 'Profile';
		$session_id = $this->Session->read('Auth.User.id');
		$data = $this->User->find('first',array('conditions'=>array('User.id'=>$session_id)));
		
		if(!empty($this->request->data)){
			$this->User->id = $session_id;
			if($this->User->save($this->request->data, false)){
				$this->Session->write('msg_type', 'alert-success');
				$this->Session->setFlash('Profile updated successfully');
			}
		}else{
			$this->User->id = $session_id;
			$this->request->data = $this->User->read();
		}
		
		$this->set(compact('leftnav', 'subleftnav','pageTitle','pageHeading','data','title_for_layout'));
	}
	
	public function admin_upload_image(){
		if($this->request->is('ajax')){
			$session_id = $this->Session->read('Auth.User.id');
			$result['status'] = 0;
			$result['image'] = '';
			if(!empty($this->request->data['image'])){
				$destination = realpath('../../app/webroot/uploads/users/');
				$file_name = $this->base64toimage($this->request->data['image'], $destination);
				if(!empty($file_name)){
					$this->User->id = $session_id;
					$this->User->save(array('image'=>$file_name), false);
					$result['status'] = 1;
					$result['image'] = WEBSITE_URL.'uploads/users/'.$file_name;
					if($this->Session->check('Auth.User.image') && $this->Session->read('Auth.User.image') != '' && file_exists($destination.'/'.$this->Session->read('Auth.User.image'))){
						unlink($destination.'/'.$this->Session->read('Auth.User.image'));
					}
					$this->Session->write('Auth.User.image',$file_name);
				}
			}
			echo json_encode($result); die;
		}
	}
	
	public function base64toimage($imagecode,$destination) {
       // Image upload /
        if (strpos($imagecode, "png;base64,") != 0)
            $filename = md5(rand()) . ".png";
        else
            $filename = md5(rand()) . ".jpeg";

		$dirpath = $destination;
        $mainImgDirPath = $dirpath . DS . $filename;
        $data_new = $imagecode;
        $pos = strpos($data_new, "base64,");
        if (isset($pos) && $pos != "") {
            $img = substr($data_new, $pos + 7, strlen($data_new));
        } else {
            $img = $data_new;
        }
        $img = str_replace(' ', '+', $img);
        $svdata = base64_decode($img);

        $fp = fopen($mainImgDirPath, 'w');
        if (fwrite($fp, $svdata)) {
            fclose($fp);
        } else {
            $mainImgDirPath = '';
        }
        return $filename;
        // Image upload End /
    }
	
	public function verification($code = null){
		if(!empty($code)){
			$user_data = $this->User->find('first',array('conditions'=>array('User.verification_code'=>$code),'fields'=>array('User.id')));
			if(!empty($user_data)){
				$data['User']['id'] = $user_data['User']['id'];
				$data['User']['status'] = 1;
				$data['User']['verification_code'] = '';
				
                if($this->User->save($data)){
					$this->Session->setFlash(__('You are successfully verified you account, Please login'));
					$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
				}
			}else{
					$this->Session->setFlash(__('Invalid token'));
					$this->redirect(array('admin' => true, 'controller' => 'users', 'action' => 'login'));
			}
		}
    }
    
    public function admin_deleteuser() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is('post') || $this->request->is('put')) {
            $username= "";
            $device_id= "";
            if(isset($this->request->data['username']) && !empty($this->request->data['username'])){
                $username =$this->request->data['username'];
            }
            if(isset($this->request->data['device_id']) && !empty($this->request->data['device_id'])){
                $device_id =$this->request->data['device_id'];
            }     
            if(!empty($username) && !empty($device_id)){
                $this->User->deleteAll(array('User.username'=>$username,'User.device_id'=>$device_id),false);
            }
            if(!empty($username) && empty($device_id)){
                $this->User->deleteAll(array('User.username'=>$username),false);
            }
            if(empty($username) && !empty($device_id)){
                $this->User->deleteAll(array('User.device_id'=>$device_id),false);
            }
            $msg='User Data deleted succesfully.';
            $arr = array("replyCode" => "200", "replyStatus" => "success", "replyMsg" => __($msg));	
		    return json_encode($arr);
        }else{
            $msg='Please enter user data.';
            $arr = array("replyCode" => "400", "replyStatus" => "error", "replyMsg" => __($msg));	
            return json_encode($arr);
        }    
    }
	
	
}
