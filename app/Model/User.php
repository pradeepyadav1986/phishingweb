<?php

class User extends AppModel {

    // User Roles 1 -> Admin 1 -> Site User
    public $name = 'User';
    //public $virtualFields = array('full_name'=>"CONCAT(User.first_name, ' ', User.last_name)");
	//public $belongsTo = array('Country'=>array('fields'=>array('id','countryName')) );
    public $validate = array(
        'name' => array(
            'name' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter full name.',
            ),
        ),	
        'email' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter email address.',
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide a valid email address.',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Email address already in use.'
            ),
        ),
		'password' => array(
                'rule1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please enter password.',
                    'last' => true
                ),
                'rule2' => array(
                    'rule' => array('minLength', 6),
                    'message' => 'The password must be 6 character long.',
                )
            )
    );

	function generatePassword($length = 8) {

        $password = "";
        $i = 0;
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }

        return $password;
    }
	
	public function checkEmail($email){
	    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return true;
		} else {
			return false;
		}
	}

}
